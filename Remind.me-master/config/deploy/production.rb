require "bundler/capistrano"
require "rvm/capistrano"                  # Load RVM's capistrano plugin.
set :whenever_command, "bundle exec whenever"
require "whenever/capistrano"
set :rvm_ruby_string, '2.1.5'        # Or whatever env you want it to run in.
set :rvm_type, :system

load 'deploy/assets'
set :user, "deployer"
set :runner, "deployer"
set :scm, :git
set :repository, "git@github.com:buffpojken/Remind.me.git"   
set :branch, "master"  
set :deploy_to, "/sites/remind.me"
set :rails_env, "production"
set :domain, "154.43.128.23"     
set :application, 'Remindme'    

role :app, domain
role :web, domain
role :db, domain, :primary => true

default_run_options[:pty] = true

ssh_options[:forward_agent] = true

set :use_sudo, false

before "deploy:update", "god:terminate_if_running"
after "deploy:update", "god:start"   

namespace :god do
  def god_is_running
    !capture("#{god_command} status >/dev/null 2>/dev/null || echo 'not running'").start_with?('not running')
  end

  def god_command
    "cd #{current_release}; bundle exec god"
  end

  desc "Stop god"
  task :terminate_if_running do
    if god_is_running
      god_config_path = File.join("#{release_path}", 'config', "#{fetch(:rails_env)}.god")

      run "#{god_command} --config-file #{god_config_path} terminate"
    end
  end

  desc "Start god"
  task :start do
    god_config_path = File.join(current_release, 'config', "#{rails_env}.god")
    environment = { :RAILS_ENV => rails_env, :RAILS_ROOT => current_release }
    run "#{god_command} -c #{god_config_path}", :env => environment
  end
end

namespace :deploy do
  task :start, :roles => [:web, :app] do
    run "cd #{current_release}; bundle exec thin -C /sites/remind.me/shared/conf/thin.yml start"
  end
  task :stop, :roles => [:web, :app] do
    run "cd #{current_release}; bundle exec thin -C /sites/remind.me/shared/conf/thin.yml stop"
  end
  task :restart, :roles => [:web, :app] do
    run "cd #{current_release}; bundle exec thin -C /sites/remind.me/shared/conf/thin.yml restart"
  end
end
