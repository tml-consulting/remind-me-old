RemindmeV2::Application.routes.draw do
  # Google OAuth
#  mount GoogleAuthentication, :at => '/auth/google'

  devise_for :users

  devise_scope :user do
    get "logout", :to => "devise/sessions#destroy"
  end

  resource :public, :only => :index
  get '/search' => "application#search"


  namespace 'admin' do 
    resources :terapeuts do 
      member do 
        get :remove
      end
    end
  end

  resource :calendar, :only => [:show, :create, :destroy], controller: 'calendars',  as: 'my_calendar' do
    collection do 
      get :events
      get :persons
    end
  end

  resources :calendars, :only => [:show, :create, :destroy] do
    member do 
      get :events 
      post :create
    end
  end

  resource :statistics do
    get '/people/:people(/from/:from(/to/:to))' => "statistics#show"
  end


  resource :dashboard, :only => :show

  resource :exports do 
    get :perform
  end

  resource :settings do 
    collection do 
      get :refresh_google_status
      post :invite_user
      get :remove_user_access
      post :reminders
    end
  end

  resources :patients do 
    collection do 
      get :autocomplete
    end
    member do
      get :activate
      post :activate
      get :session
      get :notes
      get :delete
      get :resend_invitation
    end
  end
  
  match 'smsteknik/webhook' => 'api/v1/sms#receive_sms', :as => :smsteknik, :via => :post

  namespace 'api' do 
    namespace 'v1' do 
      resource :sms, :only => [] do 
        post :status
      end
    end
  end

  root :to => 'public#index'

end
