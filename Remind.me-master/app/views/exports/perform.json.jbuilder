json.export do 

  json.user do 
    json.name   user.name
    json.email  user.email
    json.phone  user.phone
    json.role   (user.type ? user.type : 'user' )
  end

  json.events(user.events) do |event|
    json.id               event.id
    json.name             event.name
    json.description      event.description
    json.start            event.start.to_i
    json.end              event.end.to_i
    json.place            event.place
    json.all_day          event.all_day
    json.created_at       event.created_at.to_i    
    json.reminder_count   event.num_reminders
    json.status           event.reminder_status
    json.response_time    event.response_time_in_sec

    json.reminders(event.reminders) do |reminder|

      json.remind_at reminder.remind_at.to_i
      json.messages(reminder.text_messages) do |message|

        json.body       message.body
        json.sender     message.sender
        json.sent_at    message.sent_at.to_i
        json.incoming   message.incoming
        json.receiver   message.receiver
        
      end

    end
  end

end