	json.title event.name || " "
	json.place event.place || " "
	json.id event.id
	json.color event.color
	json.start event.start.to_i
	json.end event.end.to_i
	json.allDay event.all_day
	json.editable event.editable? 
	json.recurring event.recurrence_key
	# Must really find some way to handle this with Mustache, since this looks fugly! .daniel
	json.is_not_recurring event.recurrence?(:not_recurring)
	json.is_daily event.recurrence?(:daily)
	json.is_workdays event.recurrence?(:workdays)
	json.is_weekly event.recurrence?(:weekly)
	json.reminders event.reminders do |reminder|
		json.exponent reminder.exponent
		json.multiplier reminder.multiplier
		json.is_minute reminder.multiplier.eql?('m')
		json.is_hour reminder.multiplier.eql?('h')
		json.is_day reminder.multiplier.eql?('d')
	end
	json.participants(event.participants) do |participant|
		json.id participant.id
		json.name participant.name
	end