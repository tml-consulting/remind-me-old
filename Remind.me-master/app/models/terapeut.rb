class Terapeut < User

	has_many :patients, :class_name => "User", :foreign_key => "terapeut_id" do 
		def recent(limit=5)
			return order("last_session_at desc").limit(limit)
		end
	end

	def accept_terapeut_invitation_url
		return super.accept_user_invitation_url
	end

	def has_permission_for?(user)
		return self.permissions.where(["access_to_id = ?", user.id]).count > 0
	end

end
