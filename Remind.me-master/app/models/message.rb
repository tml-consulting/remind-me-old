class Message < ActiveRecord::Base
  attr_accessible :content, :user, :reminder
end
