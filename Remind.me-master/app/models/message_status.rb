class MessageStatus < ActiveRecord::Base

	attr_accessible :operator_status, :performed_at

	belongs_to :text_message

	validates :operator_status, :inclusion => {:in => SMSTeknik::ALLOWED_MESSAGE_STATES}

	def delivered?
		return operator_status.eql?(SMSTeknik::ALLOWED_MESSAGE_STATES[0])
	end

end
