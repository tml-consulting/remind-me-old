class Response < ActiveRecord::Base

	belongs_to :reminder
	belongs_to :message

  	attr_accessible :response_text, :reminder, :message
end
