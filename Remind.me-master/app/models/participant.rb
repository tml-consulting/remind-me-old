class Participant < ActiveRecord::Base

	attr_accessible :name, :user_id

	has_many :participations
	has_many :events, :through => :participations

end
