#encoding: utf-8
require 'securerandom'
class TextMessage < ActiveRecord::Base

	attr_accessible :sender, :receiver, :body, :incoming, 
		:reminder, :user, :workflow_state, :operator_identifier

	belongs_to :reminder
	belongs_to :user
	has_many :statuses, :class_name => "MessageStatus"

	validates :custom_message_id, :presence => true, :unless => lambda{|message| message.incoming? }
	validates :sender, :presence => true
	validates :receiver, :presence => true, :numericality => true

	before_validation :set_message_id, :if => lambda{|message| !message.incoming? }
	before_validation :set_sender, :if => lambda{|message| message.sender.blank? && !message.incoming? }

	# Create and send a text message. 
	# Any errors occuring during sending will be available on the returned instance of 
	# {TextMessage}.
	# @param receiver [String] String containing the phone number to send the message to 
	# @param body [String] String with the message body
	# @param sender [String] Optional. Set the number to send the message from.
	# @return [TextMessage] The created and sent TextMessage.
	def self.send_message(receiver, body, options={})
		message = TextMessage.create(:receiver => receiver, :body => body, :sender => configatron.sms.sender, :reminder => options[:reminder])
		SMSTeknik.post(message, {
			:callback_url => configatron.sms.callback_url.to_s
		})
		# Handle errors here!
		message.reload
		message
	end


	# Send the receiver using the SMSTeknik-module.
	def send_message
		SMSTeknik.post(self, {
		  :callback_url => configatron.sms.callback_url
		})    
	end

	# Save the incoming message if we can, and then update the 
	# proper dependencies, such as events and reminders!
	def handle_response(data)
		self.attributes = {
			:sender 		=> data[:orgaddr], 
			:receiver 	=> data[:destaddr], 
			:body 			=> data[:text].downcase
		}
		begin
			self.save! && self.update_reminder_status
		rescue Exception => e
			logger.error e.message
			logger.error e.backtrace
			self.errors.add(:base, "Something went wrong, invalid data passed: #{data.inspect}")
			return self
		else
			return self
		end
	end

	def update_reminder_status
		response_state, reminder = parse_state_from_body
		raise ArgumentError.new("Unknown reminder to be answered based on passed data") if reminder.nil?
		return self.update_attributes(:reminder => reminder) && reminder.answer_received(response_state)
	end

	# Update the current status of the receiver. 
	# Called as part of the API-based callbacks from the textmessage operator. 
	#
	# @param status_hash [Hash] A hash containing :state, :timestamp, :ref_id.
	# @option status_hash [String] :state String from the list of possible states as referenced by the operator
	# @option status_hash [String] :timestamp The time of the last status change
	# @option status_hash [String] :ref_id The reference id returned by the operator when sending
	# @return [Boolean] Success
	def update_status(status_hash)
		status = self.statuses.create(:operator_status => status_hash[:state], 
																	:performed_at => status_hash[:timestamp])
		return status.errors.empty? 
	end

	def delivered? 
		self.statuses.last.delivered? 
	end

	private

	def set_message_id
		self.custom_message_id = SecureRandom.urlsafe_base64(10)
	end

	def set_sender
		self.sender = configatron.sms.sender
	end

	def parse_state_from_body
		matches = self.body.match(configatron.sms.answer_regexp)
		raise ArgumentError.new("Parser could not extract proper reminder-data from sms body") if matches.length != 3
		answer_status = determine_status_from_match(matches[1])
		reminder 	= Reminder.where(:message_identifier => matches[2]).order("created_at desc").first
		return answer_status, reminder
	end

	def determine_status_from_match(matched_response)
		if matched_response.match(/[aj]{2}/)
			return :answered_yes
		elsif matched_response.match(/[nej]{3}/)
			return :answered_no
		else
			return :no_answer
		end
	end

end