class Permission < ActiveRecord::Base
	belongs_to :user 
	belongs_to :access_to, :class_name => 'User'

	attr_accessible :user, :access_to


	validates :user, :presence => true
	validates :access_to, :presence => true

end
