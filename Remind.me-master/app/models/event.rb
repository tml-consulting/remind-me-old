class Event < ActiveRecord::Base

	acts_as_paranoid

	bitmask :recurrence, :as => [:not_recurring, :daily, :weekly, :workdays], :default => :not_recurring

	# This should be moved to configatron. .daniel
	@@reminder_statuses = { 
		1 => :answered_yes,
		2 => :answered_no,
		3 => :no_answer 
	}
	@@reminder_status_color = {:answered_yes => '#199b26', :answered_no => '#bd1b2e', :no_answer  => '#575555'}
	# End of configuration .daniel 

	belongs_to :user
	belongs_to :creator, :class_name => 'User'

	# TODO: Make this use acts_as_paranoid so that users can't delete their stats! .daniel
	# Shouldn't this be dependent? 
	has_many :reminders

	has_many :participations
	has_many :participants, :through => :participations


	attr_accessible :name, :description, :start, :end, :remote_id, :place, :all_day, :num_reminders, 
	:reminder_offset, :user, :user_id, :reminder_status, :response_time_in_sec, :num_reminders_required,
	 :reminder_list, :creator, :recurrence_key, :edit_all_recurring, :recurrence_identifier, :skip_recurring_steps, 
	 :participant_list, :skip_reminders

	after_create :sync_with_google
	before_validation :set_creator_if_missing
	before_validation :set_recurrence_identifier
	before_validation :unlink_event_if_recurring_edit_flag_is_set, :on => :update
	before_validation :set_participants
	before_validation :enforce_date_difference
	after_create :set_default_reminders, :if => lambda{|event| event.reminders.empty? }
	after_create :create_recurring_events, :unless => lambda{|event| event.recurrence?(:not_recurring) || event.skip_recurring_steps }
	after_update :sync_with_google
	after_update :update_recurring_reminders, :if => lambda{|event| !event.recurrence?(:not_recurring) && !event.skip_recurring_steps && event.edit_all_recurring }

	# Change this to an update to support the case where one event is converted on the fly!
	after_update :update_recurring_events, :if => lambda{ |event| !event.recurrence?(:not_recurring) && !event.skip_recurring_steps && (event.start_changed? || event.recurrence_changed? || event.end_changed? || event.name_changed? || event.place_changed?)}
	
	before_destroy :unlink_event_if_recurring_edit_flag_is_set
	after_destroy :remove_from_google
	after_destroy :remove_linked_events, :if => lambda{|event| !event.recurrence?(:not_recurring) && !event.skip_recurring_steps }

	after_save :update_reminders, :if => lambda{|event| event.start_changed? }

	validates :user, :presence => true
	validates :creator, :presence => true

	validates :start, :presence => true
	validates :end, :presence => true

	scope :without_answers, -> { where(["reminder_status = ?", Event.status(:no_answer)]) }


	attr_accessor :skip_recurring_steps, :participant_list
	attr_accessor :edit_all_recurring


	def update_with_google_event(google_event)
		self.name 			= google_event.summary
		self.description 	= google_event.description 
		self.start 			= google_event.start.dateTime
		self.end 			= google_event.end.dateTime
		self.remote_id 		= google_event.id
	end

	def recurrence_key=(recurrence_key)
		case recurrence_key
		when 'n'
			self.recurrence = :not_recurring
		when 'd'
			self.recurrence = :daily
		when 'wd'
			self.recurrence = :workdays
		when 'we'
			self.recurrence = :weekly
		else
			self.recurrence = :not_recurring
		end
	end

	def recurrence_key
		case recurrence
		when [:not_recurring]
			'n'
		when [:daily]
			'd'
		when [:workdays]
			'wd'
		when [:weekly]
			'we'
		else
			'n'
		end		
	end

	def set_participants
		Event.transaction do 
			self.participants.delete_all
			(participant_list || "").split(",").each do |participant|
				participant_object = self.creator.participants.where(:name => participant).first_or_create
				logger.info participant_object.inspect
				self.participants << participant_object
			end
		end
	end

	def reminder_list=(reminder_definition)
		Reminder.transaction do 
			self.reminders.destroy_all
			return if reminder_definition.blank? 			
			reminder_definition.each_pair do |index, reminder_data|
				unless reminder_data[:exponent].blank? || reminder_data[:multiplier].blank?
					self.reminders.new(:exponent => reminder_data[:exponent], :multiplier => reminder_data[:multiplier])
				end
			end
			# Keep a reference to this, if we'll need to create reminders for any recurring events
			@reminder_definition = reminder_definition
		end
	end

	def mark_as_answered!(state, reminder)
		self.status = state
		self.attributes = {
			:num_reminders_required => reminder.calculate_previously_sent_reminders, 
			:response_time_in_sec	=> reminder.calculate_response_time,
			:num_reminders 			=> self.reminders.length
		}
		return self.save
	end

	def status=(status)
		self.reminder_status = @@reminder_statuses.invert.fetch(status, @@reminder_statuses[:no_answer])
	end

	def self.status(status_key)
		return @@reminder_statuses.invert[status_key]
	end

	def expired?
		self.reminder
	end

	def self.expire!(timestamp)
		self.where(["end < ? and reminder_status IS NULL", timestamp.utc]).each do |event|
			event.expire!
		end
	end


	def expire!
		self.status = :no_answer
		self.reminders.each{|reminder| reminder.expire! }
		self.save
	end


	def get_reminder_status
		return reminder_status[self.reminder_status]
	end

	def answer_yes?
		return reminder_status == Event.status(:answered_yes)
	end

	def answer_no?
		return reminder_status == Event.status(:answered_no)
	end

	def no_answer?
		return reminder_status == Event.status(:no_answer)
	end

	def editable? 
		return Time.now.localtime < start.localtime
	end

	def length_in_seconds
		(self.end - self.start)
	end

	def color
		case reminder_status
			when 1
				@@reminder_status_color[:answered_yes]
			when 2
				@@reminder_status_color[:answered_no]
			when 3
				@@reminder_status_color[:no_answer]
			else
				"#6f02c9"
		end
	end

	def extend_calculated_recurring_events(opts)
		create_recurring_events(opts)
	end

	def dependent_events
		Event.where(["recurrence_identifier = ?", self.recurrence_identifier])
	end

	private

	def set_creator_if_missing
		self.creator = self.user if self.creator.nil?
	end

	def sync_with_google
		return unless user.has_google_account?
		Resque.enqueue(GoogleEventSync, self.id)
	end

	def remove_from_google
		return unless user.has_google_account?
		Resque.enqueue(GoogleEventRemover, {'remote_id' => self.remote_id, 'user_id' => user.id}.to_json)		
	end

	def update_reminders
		self.reminders.each(&:refresh)
	end

	def create_recurring_events(opts = nil)
		if opts.nil?
			opts = {:time_frame => {:months => 6}, :delete => false, :start => self.start, :step => self.recurrence}
		end
		# The delete-function here is problematic, since it creates erratic behaviour when editing events in the middle
		# of a chain. Somehow, we need to rebuild this to match up the proper events anyhow!
		logger.info @reminder_definition.inspect
		Event.transaction do 
			if opts[:delete]
				events = Event.where(["recurrence_identifier = ? and reminder_status is null", self.recurrence_identifier])
				events.map!{|event| event.destroy }
			end
			dates_to_add = RecurrenceManager.plot(opts[:start], opts[:step], opts[:time_frame])
			# Remove the first date, since it will be equal to the event start date (we don't want duplicates!)
			dates_to_add.slice!(0, 1)
			dates_to_add.each do |recurrence_date|
				recurring_event = Event.create(	:name 	=> self.name, 
																				:place 	=> self.place, 
																				:start 	=> recurrence_date, 
																				:end 		=> recurrence_date.advance(:seconds => self.length_in_seconds), 
																				:user 	=> self.user, 
																				:recurrence_key => self.recurrence_key, 
																				:reminder_list => @reminder_definition,
																				:recurrence_identifier => self.recurrence_identifier, 
																				:skip_recurring_steps => true)			
				logger.info recurring_event.errors.inspect
			end
		end
	end

	def update_recurring_events
		events_to_update 	= Event.with_deleted.where(["recurrence_identifier = ? and start > ?", self.recurrence_identifier, Time.now.localtime]).order('start')
		return if events_to_update.empty? 
		dates_to_add 		= RecurrenceManager.plot(events_to_update.first.start, self.recurrence, {:months => 6})
		dates_to_add.each_with_index do |recurrence_date, idx|			
			event = events_to_update[idx]
			return if event.nil?
			next if event.destroyed?
			adjusted_date = Time.utc(recurrence_date.year, recurrence_date.month, recurrence_date.day, self.start.hour, self.start.min)
			event.update_attributes({
				:name 									=> self.name, 
				:place 									=> self.place, 
				:start 									=> adjusted_date, 
				:end 										=> adjusted_date.advance(:seconds => self.length_in_seconds), 
				:skip_recurring_steps 	=> true
			})
		end
	end

	def update_recurring_reminders
		return if @reminder_definition.eql?(nil)
		events = Event.where(["recurrence_identifier = ?", self.recurrence_identifier])
		Event.transaction do 
			events.each do |event|
				event.reminder_list = @reminder_definition
				event.skip_recurring_steps = true
				event.save
			end
		end
	end

	def remove_linked_events
		Event.transaction do 
			events = Event.where(["recurrence_identifier = ? and start > ?", self.recurrence_identifier, Time.now])
			events.map{|event| event.skip_recurring_steps = true; event.destroy}
		end
	end

	def set_default_reminders
		Event.transaction do 
			self.user.default_reminders.each do |reminder|
				self.reminders.create(:exponent => reminder.exponent, :multiplier => reminder.multiplier)
			end
		end
	end

	def enforce_date_difference
		return if self.start.nil? || self.end.nil?
		if self.end - self.start < 60.0 
			self.end = self.start.advance(:minutes => 30)
		end
	end

	def unlink_event_if_recurring_edit_flag_is_set
		if self.edit_all_recurring == "false" && self.recurrence_key != 'n'
			self.recurrence_identifier = nil
			self.recurrence_key = 'n'
		end
	end

	def set_recurrence_identifier
		self.recurrence_identifier = rand(36**8).to_s(36) if self.recurrence_identifier.blank?
	end

end