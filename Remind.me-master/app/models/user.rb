class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :invitable, :invite_for => 2.weeks

  
  validates :phone, :presence   => true, :if => lambda{|a| a.instance_of?(User)}
  validates :phone, :uniqueness => true, :if => lambda{|a| a.instance_of?(User)}
  validates :processing_consent_given, :acceptance => {:accept => true} , allow_nil: false

  validates :email, :uniqueness => true, :presence => true

  before_validation :set_processing_consent
  before_save :reformat_cellphone_number, :if => lambda{|u| u.phone_changed? }

  has_many :events, :dependent => :destroy
  has_many :participants, :dependent => :destroy
  has_many :messages, :dependent => :destroy
  has_many :default_reminders, -> { order('remind_at desc') }, :class_name => 'Reminder', :dependent => :destroy

  has_many :permissions, :dependent => :destroy
  has_many :permitted, :foreign_key => 'access_to_id', :class_name => "Permission"

  has_many :permission_to, :through => :permissions, :source => 'access_to'

  has_many :given_permission_to, :through => :permitted, :source => 'user'

  belongs_to :terapeut

  has_many :created_events, :class_name => 'User', :foreign_key => 'creator_id'

  attr_accessible :email, :password, :password_confirmation, :remember_me, :processing_consent_given
  attr_accessible :login, :name, :phone, :terapeut, :messages, :events, :can_be_accessed_by, :user_identifier

  def has_google_account? 
    return false
  end

  def user_identifier 
    return name || email
  end

  def events_for_range(start, stop)
    self.events.where(["start > ? OR end < ?", start, stop])
  end


  def has_role?(user)
    return true if user == :terapeut && self.is_a?(Terapeut)
    return true if user == :admin && self.is_a?(Admin)
    return true if user == :viewer && self.is_a?(Viewer)

    return true if user == :user && self.instance_of?(User)
  	return false
  end

  # Replace the existing default reminders for this user with the 
  # reminder-definitions in the passed list. 
  def set_default_reminders(reminder_list)
    User.transaction do 
      self.default_reminders.destroy_all
      (reminder_list || {}).each_pair do |index, reminder_data|
        self.default_reminders.create(:exponent => reminder_data[:exponent], :multiplier => reminder_data[:multiplier])
      end    
    end
  end

  def get_export_data
    
  end

  private   

  def set_processing_consent
    self.processing_consent_given = (self.processing_consent_given == "1")
    logger.info self.inspect
  end

  def reformat_cellphone_number
    # This number is already on a decent format!
    return if phone.match(/46\d{9}/)
    if phone.match(/07\d{8}/)
      self.phone = phone.gsub(/^\d{1,1}/, '46')
    elsif phone.match(/07\d{1}-\d{3}\s\d{2}\s\d{2}/)
      match_data = phone.match(/07(\d{1})-(\d{3})\s(\d{2})\s(\d{2})/)
      self.phone = "467"+match_data.captures.join()
    elsif phone.match(/07(\d{1})-(\d{7})/)
      match_data = phone.match(/07(\d{1})-(\d{7})/)
      self.phone = "467"+match_data.captures.join()
    end
  end

end
