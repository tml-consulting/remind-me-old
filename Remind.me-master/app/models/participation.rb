class Participation < ActiveRecord::Base

	attr_accessible :event, :participant

	belongs_to :participant
	belongs_to :event

end
