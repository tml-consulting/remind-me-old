#coding:utf-8
class Reminder < ActiveRecord::Base
  include Workflow

	belongs_to :event
  belongs_to :user
	has_many :responses
  has_many :text_messages do 

    def first_incoming
      where(["incoming = ?", true]).limit(1).order('created_at').first
    end

    def last_outgoing
      where(["incoming = ?", false]).limit(1).order('created_at desc').first
    end

  end
  attr_accessible :event, :responses, :remind_at, :exponent, :multiplier

  before_save :update_remind_at
  before_create :set_message_identifier

  workflow do 
    state :active do 
      event :mark_as_sent, :transitions_to => :awaiting_response
      event :sending_error_occured, :transitions_to => :sending_error
      event :expire, :transitions_to => :expired
    end

    state :awaiting_response do 
      event :mark_as_answered, :transitions_to => :finished
      event :expire, :transitions_to => :expired
    end

    state :sending_error do 
      event :resend, :transitions_to => :awaiting_response
      event :expire, :transitions_to => :expired
    end
    state :expired
    state :finished
  end

  def refresh
  	update_remind_at
  	save
  end

  # Fetch all pending reminders within the passed time bounds and send them. 
  # This method fetches all the pending reminders, send them and then update
  # internal status of each reminder accordingly.
  # @return [Hash] A status-hash containing relevant status info
  # @param [Time] The future bound of reminders to be sent.
  def self.send_reminders(timestamp)
    status = {:flawless_run => true, :faulty_reminders => [], :completed_reminders => []}
    reminders = self.where(["remind_at < ? and workflow_state = ?", timestamp, 'active'])
    status[:reminders_processed] = reminders.length
    reminders.each do |reminder|
      begin
        reminder.send_reminder
      rescue Exception => e
        status[:flawless_run] = false
        status[:faulty_reminders] << reminder
        logger.error e.message
        logger.error e.backtrace
        reminder.sending_error_occured!
      else
        status[:completed_reminders] << reminder
      end
    end
    status
  end

  # Generate a {{TextMessage}} bound to this reminder, and send it.
  def send_reminder
    textmessage = self.text_messages.new(:receiver => self.event.user.phone)
    textmessage.body = %{Kom ihåg #{event.name}, som händer #{I18n.localize(event.start.localtime, :format => "%A, %H:%M")}. Svara: ja/nej #{self.message_identifier.to_s} för att bekräfta}
    textmessage.save! 
    textmessage.send_message
    self.mark_as_sent!
  end

  def answer_received(state)
    self.event.mark_as_answered!(state, self)    
    self.mark_as_answered! if self.can_mark_as_answered?
  end

  def calculate_previously_sent_reminders
    return self.event.reminders.where(["remind_at <= ? and workflow_state = ?", self.remind_at, 'awaiting_response']).count
  end

  def calculate_response_time
    return self.text_messages.first_incoming.created_at.to_i - self.text_messages.last_outgoing.created_at.to_i
  end

  def to_s
    %{#{exponent} #{multiplier_as_string} före}
  end

  private

  def multiplier_as_string
    case multiplier
      when 'm'
        return 'minuter'
      when 'h'
        return 'timmar'
      when 'd'
        return 'dagar'
      else
        return ''
    end
  end

  def update_remind_at
    start_time = self.event.nil? ? Time.now : self.event.start
    self.remind_at = (start_time || Time.now) + calculate_offset
  end

  def calculate_offset
  	case multiplier
	  	when 'm'
	  		return -(exponent.minutes)
	  	when 'h'
	  		return -(exponent.hours)
	  	when 'd'
	  		return -(exponent.days)
	  	else
	  		return 0
	  	end
  end

  def set_message_identifier
    begin 
      self.message_identifier = rand(9999)
    end while Reminder.exists?(["workflow_state in (?) and message_identifier = ?", [:active, :awaiting_response], self.message_identifier])
  end

end