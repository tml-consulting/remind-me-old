#coding:utf-8
class Invitations < Devise::Mailer 
  default from: "robot@monkeydancers.com"

  layout 'email'

 def invitation_instructions(record, token, opts={})
  	logger.error("Trying to send invitation" + record.class.to_s)
  	if record.instance_of?(User)
  		user_invitation(record, token, opts)
		elsif record.instance_of?(Viewer)
			viewer_invitation(record, token, opts)
		elsif record.instance_of?(Terapeut)
			terapeut_invitation(record, token, opts)
		end
  end

  def user_invitation(record, token, opts = {})
  	@resource = record
  	mail(:to => @resource.email, :subject => "Du har blivit inbjuden till Remindme!") do |format|
  		format.html{ render 'invitations/user_invitation' }
  		format.text{ render 'invitations/user_invitation' }
  	end
  end

  def viewer_invitation(record, token, opts = {})
  	@resource = record
  	mail(:to => @resource.email, :subject => "Du har blivit inbjuden till Remindme!") do |format|
  		format.html{ render 'invitations/viewer_invitation' }
  		format.text{ render 'invitations/viewer_invitation' }
  	end
  end

  def terapeut_invitation(record, token, opts = {})
  	@resource = record
  	mail(:to => @resource.email, :subject => "Du har blivit inbjuden till Remindme!") do |format|
  		format.html{ render 'invitations/viewer_invitation' }
  		format.text{ render 'invitations/viewer_invitation' }
  	end
  end

end
