#coding:utf-8
class UserMailer < ActionMailer::Base
	default from: "robot@monkeydancers.com"

	layout 'email'

	def send_access_notification(from, to)
	  	@form 		= from
		@resource 	= to
	  	mail(:to => @resource.email, :subject => "Remindme: Du har fått tillgång till en kalender") do |format|
		    format.html { render "invitations/access_notification" }
		    format.text { render "invitations/access_notification" }
		end
  	end


  		def send_removed_access_notification(from, to)
	  	@form 		= from
		@resource 	= to
	  	mail(:to => @resource.email, :subject => "Remindme: Du har blivit borttagen från en kalender") do |format|
		    format.html { render "invitations/removed_access_notification" }
		    format.text { render "invitations/removed_access_notification" }
		end
  	end
end
