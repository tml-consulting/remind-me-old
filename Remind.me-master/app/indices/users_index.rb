ThinkingSphinx::Index.define :user, :with => :active_record do
	indexes :name
	indexes :email
	indexes :phone
	has :terapeut_id
end 