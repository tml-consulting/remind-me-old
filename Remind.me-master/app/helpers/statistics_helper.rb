module StatisticsHelper
 def trend_sign(value)  
 	if value.nil?
 		raw %{<img src="#{ asset_path 'statistics/trend_unknown.png' }" alt="?" style="float: right; margin: 0px 0px 6px 6px"/>}
	else 

	  if value > 0

	    raw %{<img src="#{ asset_path 'statistics/trend_up.png' }" alt="possitiv trend" style="float: right; margin: 0px 0px 6px 6px"/>}
	  elsif value < 0
	    raw %{<img src="#{ asset_path 'statistics/trend_down.png' }" alt="negativ trend" style="float: right; margin: 0px 0px 6px 6px"/>}        
	  else
	    raw %{<img src="#{ asset_path 'statistics/trend_neutral.png' }" alt="neutral trend" style="float: right; margin: 0px 0px 6px 6px"/>}                
	  end
	end
end

end
