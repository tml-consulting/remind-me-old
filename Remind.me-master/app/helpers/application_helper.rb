module ApplicationHelper

	# Device Mappings
	def resource_name
		:user
	end

	def resource 
		@resource ||= User.new
	end

	def devise_mapping
		@device_mapping ||= Devise.mappings[:user]
	end

	def highlight(current, matcher, value)
    	return value if matcher =~ current
  	end

end
