class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :authenticate_user!, :only => [:search]

  layout :layout_by_resource

  before_filter :update_sanitized_params, if: :devise_controller?

  def update_sanitized_params    
    devise_parameter_sanitizer.for(:accept_invitation) {|u| 
      u.permit(:phone, :invitation_token, :processing_consent_given, :password, :password_confirmation)      
    }
    logger.info params.inspect
  end

   	def admin_required
		unless current_user && current_user.has_role?(:admin)
			redirect_to '/' and return
		end
	end
	

	def admin_or_terapeut_required
		if current_user && (current_user.has_role?(:terapeut) || current_user.has_role?(:admin))
			return true
		else 
			redirect_to '/' and return false
		end
	end	

  	def terapeut_required
		unless current_user && current_user.has_role?(:terapeut)
			redirect_to '/' and return
		end
	end


	def search
    if current_user.has_role?(:admin)
      render :text =>  User.search("*%s*" % params[:query]).to_json and return      
    else
      render :text => User.search("*%s*" % params[:query], :with => {:terapeut_id => current_user.id}).to_json and return
    end
  end
	
	protected

	  def layout_by_resource
	    if devise_controller?
	      "public"
	    else
	      "application"
	    end
	  end

end
