#coding:utf-8
class PatientsController < ApplicationController

	# This is incompleate and need to bee adapted to the new user structure

	before_filter :admin_or_terapeut_required

	def index
		@patients = current_user.patients.order("last_name").paginate(:per_page => 25, :page => params[:page])
	end
	
	def new
		@patient = User.new
	end
	
	def show
		@patient = current_user.patients.find(params[:id])		
	end
	
	def edit
		@patient = current_user.patients.find(params[:id])
	end
	
	def create

		@patient = current_user.patients.invite!({:email => params[:patient][:email], :name => params[:patient][:name], :phone => params[:patient][:phone]}, current_user) do |p|
			p.given_permission_to << current_user 
			p.terapeut = current_user
		end
			

		# @patient = current_user.patients.create(params[:patient])

		if @patient.errors.empty? 
			# redirect_to activate_patient_path(@patient) and return
			redirect_to patients_path and return

		else
			flash[:error] = "Det gick inte att spara, se nedan!"
			render :action => :new and return
		end

	end
	
	def update
		@patient = @current_user.patients.find(params[:id])
		if @patient.update_attributes(params[:patient])
			flash[:notice] 	= "#{@patient.name} har uppdaterats"
			redirect_to edit_patient_path(@patient) and return
		else
			flash[:error] 	= "Det gick inte att spara, se nedan!"
			render :action => :edit and return
		end
	end

	def resend_invitation
		begin
			u = User.find(params[:id])
			u.invite!() 
			flash[:notice] = "Ny inbjudan skickad"
			redirect_to :back

		rescue ActiveRecord::RecordNotFound
			redirect_to :back
		end
	end

	def delete
		@patient = User.find(params[:id])		
		if can?(:destroy, @patient) && @patient.destroy
			flash[:notice] = "Användare borttagen."
		else
			flash[:error] = "Du har inte möjlighet att genomföra detta."
		end
		redirect_to patients_path and return 
	end


	# Den här vet jag inte hur den är konstruerad, DS?
	def autocomplete
	  tags = current_user.owned_tags.where(["name like ?", '%'+params[:term]+'%'])
		render :text => tags.map{|t| t.name}.to_json, :status => 200 and return
	end


end

