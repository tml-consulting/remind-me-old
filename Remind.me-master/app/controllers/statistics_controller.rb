#coding:utf-8
class StatisticsController < ApplicationController
	before_filter :authenticate_user!

	def show
		# Create a user friendly date calculator
		@date_string = ""
		@start_date  = params[:from]
		@end_date    = params[:to]

		if @start_date == "last_month"
			@start_date   = 1.month.ago.beginning_of_month
			@end_date     = 1.month.ago.end_of_month
			@date_string  = "Förra månaden"
		elsif @start_date == "this_month"
			@start_date = Time.now.beginning_of_month
			@end_date   = Time.now
			@date_string = "Denna månad"       
		elsif @start_date == "this_week"
			@start_date = Time.now.beginning_of_week
			@end_date   = Time.now
			@date_string = "Denna vecka"     
		elsif @start_date == "last_week"
			@start_date = Time.now.beginning_of_week - 7.days
			@end_date   = Time.now.beginning_of_week
			@date_string = "Förra veckan"
		else  
			begin 
				@start_date  = Date.parse(@start_date)
				@end_date    = Date.parse(@end_date)
				@date_string = "Perioden " + @start_date.to_s(:db) + " till " +  @end_date.to_s(:db)
			rescue Exception => e
	    	  # Last week is default timewindow
	    	  params[:from] = "this_week"
				@start_date = Time.now.beginning_of_week
				@end_date   = Time.now
				@date_string = "Denna vecka"         
    	end
    end

    ## Parse user params
    report_for = nil
    if params[:people].blank?
	    report_for 						= current_user.id.to_i
	else 
		if params[:people] =~ /^\d+$/	
			begin
				u = User.find(params[:people])
				# This should perhaps be generalized in a separate method
				report_for = u.id.to_i if current_user.has_role?(:admin) || u == current_user || current_user.permission_to.include?(u)
			rescue  ActiveRecord::RecordNotFound
				report_for = nil
			end
		elsif  params[:people] =~ /\d+\,\d+/
			report_for = []
			params[:people].split(',').each do |u|
				begin
					u = User.find(u)
					report_for.push(u.id.to_i) if current_user.permission_to.include?(u)
				rescue  ActiveRecord::RecordNotFound
				end
			end
		end
	end


	unless report_for.nil? || (report_for.is_a?(Array) && report_for.empty?)
   		@report            				= GenerateReport.generate_report(@start_date, @end_date, report_for )
    	period_in_days 					= (@end_date - @start_date).to_i
    	@compare_period_report          = GenerateReport.generate_report(@start_date - period_in_days, @start_date,  report_for )
    	@comparison 					= GenerateReport.generate_comparison(@report, @compare_period_report)
    end




 end # End of show

end
