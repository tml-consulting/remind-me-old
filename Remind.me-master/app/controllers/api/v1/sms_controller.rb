class Api::V1::SmsController < ApplicationController

	# TODO: Add some kind of authentication here, preferably IP-based.
	def status
		message = TextMessage.find_by_custom_message_id(params[:customid])
		respond_to do |format|
			if message && message.update_status({:state => params[:state], :timestamp => params[:datetime], :ref_id => params[:ref]})
				format.html{ render :nothing => true, :status => 200 and return }
			else
				format.html{ render :nothing => true, :status => 400 and return }
			end
		end
	end


	def receive_sms
		message = TextMessage.new(:incoming => true)
		message.handle_response(params)
		respond_to do |format|
			if message && message.errors.empty? 
				format.html{ render :nothing => true, :status => 200 and return }
			else
				format.html{ render :nothing => true, :status => 400 and return }
			end
		end

	end

end
	