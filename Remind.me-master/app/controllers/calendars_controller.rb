require 'time'
class CalendarsController < ApplicationController
	before_filter :authenticate_user!
	before_filter :check_calendar_access


	def show
	end

	# Update this to include reminders in the existing sweep - right now we're using n*n+1 	
	def events
		start_time, stop_time = Time.at(params[:start].to_f), Time.at(params[:end].to_f)
		@events = @active_calendar.events_for_range(start_time, stop_time)
		respond_to do |page| 
			page.json {}
		end 
	end

	# Note implemented and tested yet
	def create
		if params[:event][:event_id].nil?
			params[:event][:user] = @active_calendar
			event = Event.create(params[:event]);
		else 
			event = @active_calendar.events.find(params[:event][:event_id])
			event.update_attributes(params[:event].except(:event_id))
			logger.info event.inspect
			event.save
		end 
		logger.info event.errors.inspect
		respond_to do |format|
			if event && event.errors.empty? 				
				format.json{ render :partial => 'event', :locals => {:event => event} and return }
			else
				format.json{ render :text => {:error => true, :event => event}.to_json and return }
			end
		end
	end
	
	def destroy
		event = @active_calendar.events.find_by_id(params[:event][:event_id])
		# This flag is required to make sure the event automatically knows if it should delete itself
		# or all instances of a recurring event with the same identifier. .daniel
		event.edit_all_recurring = params[:event][:edit_all_recurring]
		if event && event.destroy
			render :text => {:error => false, :event => event}.to_json and return
		else
			render :text => {:error => true, :event => event}.to_json and return
		end
	end

	def persons
		@hits = @active_calendar.participants.where(["name like ?", %{%#{params[:q]}%}])
		respond_to do |format|
			format.json{}
		end
	end

	private 

	def check_calendar_access
		unless params['id'].nil?
			if User.exists?(params['id']) && (current_user.id == params['id'].to_i || current_user.has_role?(:admin) || current_user.permission_to.include?(User.find(params[:id])))
				@active_calendar = User.find(params[:id]) 
			else 
				if current_user.has_role?(:user)
					redirect_to dashboard_path()
				else
					redirect_to dashboard_path()
				end
			end
		else 
			@active_calendar = current_user
		end
	end

end
