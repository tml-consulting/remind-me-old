#coding:utf-8
class Admin::TerapeutsController < ApplicationController

  before_filter :admin_required

  def index
    @terapeuts = Terapeut.paginate(:per_page => 15, :page => params[:page])
  end
  
  def new
    @terapeut = Terapeut.new
  end

  def create
    if User.exists?(:email => params[:terapeut][:email])
      @terapeut = Terapeut.new
      @terapeut.errors.add(:email, :taken)
      render :action => :new and return
    else
      @terapeut = Terapeut.invite!(params[:terapeut], current_user)

      # REDO
      if @terapeut.save 
        flash[:notice] = "Stödpersonen har sparats och ett email har skickats!"
        redirect_to admin_terapeuts_path and return
      else
        render :action => :new and return
      end
    end
  end

  def edit
    @terapeut = Terapeut.find(params[:id])
  end
  
  def update
    @terapeut = Terapeut.find(params[:id])
    if @terapeut.update_attributes(params[:terapeut])
			flash[:notice] 	= "#{@terapeut.name} har uppdaterats"
			redirect_to params[:redirect_to] || edit_admin_terapeut_path(@terapeut) and return
		else
			flash[:error] 	= "Det gick inte att spara, se nedan!"
      		render :action => :edit and return
		end		
  end
  
  def remove
    terapeut = User.find(params[:id])
    if terapeut && terapeut.delete
      flash[:notice] = "#{terapeut.name} har tagits bort"
      redirect_to admin_terapeuts_path and return
    else
      flash[:error] = "Vi kunde inte ta bort #{terapeut.name}, försök igen!"
      redirect_to edit_admin_terapeut_path(terapeut) and return      
    end
  end

end
