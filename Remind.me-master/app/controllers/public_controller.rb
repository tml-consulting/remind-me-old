class PublicController < ApplicationController
	layout 'public'
	def index
		if current_user
			if current_user.has_role?(:user)
				redirect_to my_calendar_path  
			else 
				redirect_to dashboard_path
			end
		end 
	end

end
