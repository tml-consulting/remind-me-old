#coding:utf-8
class SettingsController < ApplicationController
	before_filter :authenticate_user!
	before_filter :setup_deferred_editing, :only => [:reminders]

	def show
		@users_with_access_to_user = current_user.given_permission_to
	end

	def edit
		@user = current_user
	end

	def update
		respond_to do |format|
			if current_user.update_attributes(params[:user]) 
				flash[:notice] = "Information uppdaterad."
				format.html{ redirect_to settings_path and return }
			else
				flash[:error] = "Något gick tyvärr fel!"
				logger.info current_user.errors.inspect
				format.html{ @user = current_user; render :action => :edit and return }				
			end
		end
	end

	def reminders
		respond_to do |format|
			if @user.set_default_reminders(params[:reminders])
				format.html{ render :partial => 'reminder_list', :locals => {:user => @user} }
			else
				format.html{ render :nothing => true, :status => 500 and return }
			end
		end
	end

	def remove_user_access
		u = User.find(params[:user_id])

		if u.nil?
			redirect_to :action => :show and return
		else 
			if current_user.given_permission_to.delete(u)
				UserMailer.send_removed_access_notification(current_user, u).deliver_now
			end
			redirect_to :action => :show and return
		end		
	end

	def invite_user

		u = User.where(:email => params[:email].strip).first

		if params[:email].blank?
			flash[:error] = "Du måste ange en email."
			redirect_to :action => :show and return
		elsif u.nil? 
			u = Viewer.invite!({:email => params[:email]}, current_user)
			u.update_attribute('type', 'Viewer')
			Permission.create(:user => u, :access_to => current_user)
			flash[:notice] = "Inbjudan skickad"		
		else
			if u == current_user
				flash[:notice] = "Du kan inte lägga till dig själv"
				redirect_to :action => :show and return
			elsif current_user.given_permission_to.include?(u)
				flash[:notice] = "Användaren har redan tillgång till din kalender"
				redirect_to :action => :show and return
			end
			Permission.create(:user => u, :access_to => current_user)
			flash[:notice] = "Användare tillaggd"
			# Här borde vi skicka ett email eller skapa en notification
			UserMailer.send_access_notification(current_user, u).deliver_now
		end
		redirect_to :action => :show and return
	end

	def refresh_google_status
		render :partial => 'google_account' and return
	end

	private

	def setup_deferred_editing
		tentative_user = User.where(["id = ?", params[:patient_id]]).first
		if current_user.has_role?(:terapeut) && current_user.has_permission_for?(tentative_user)
			@user = tentative_user
		else
			@user = current_user
		end
	end
end
