class ExportsController < ApplicationController

  before_filter :authenticate_user!

  def perform

    @user = params[:user_id] ? User.find(params[:user_id]) : current_user
  
    data = render_to_string(template: 'exports/perform.json.jbuilder', locals: {user: @user})
    send_data data, :disposition => :attachment, :filename => "remindme.export.json", :type => "application/json"
  end

end