// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
//= require raphael/raphael-min.js
//= require raphael/g.raphael-min.js
//= require raphael/g.pie-min.js


function setup(pie_data1, pie_data2){
	
	$('.sort-on-answer').click(function(){ 
			$('.calendar-list>div').tsort({attr: 'data-answer'});
			$('.calendar-list>.header').css({display: 'none'});

	});

	 $('.sort-on-date').click(function(){ 
	 	$('.calendar-list>.header').css({display: 'block'});
		$('.calendar-list>div,h4').tsort({attr: 'data-date'});

	 });

	$.datepicker.setDefaults($.datepicker.regional['sv']);

	$( "#start_date" ).datepicker({dateFormat: 'yy-mm-dd'} );
	$( "#end_date" ).datepicker({dateFormat: 'yy-mm-dd'} );;

	var r2 = Raphael("where_holder"),
	pie = r2.piechart(55, 70, 50, pie_data1, { legend: ['Själv ', 'annan'], legendpos: "east", colors: ['#6d01c6', '#575555' ]});

	pie.hover(function () {
		this.sector.stop();
		this.sector.scale(1.1, 1.1, this.cx, this.cy);

		if (this.label) {
			this.label[0].stop();
			this.label[0].attr({ r: 7.5 });
			this.label[1].attr({ "font-weight": 800 });
		}
	}, function () {
		this.sector.animate({ transform: 's1 1 ' + this.cx + ' ' + this.cy }, 500, "bounce");

		if (this.label) {
			this.label[0].animate({ r: 5 }, 500, "bounce");
			this.label[1].attr({ "font-weight": 400 });
		}
	});




	var r = Raphael("holder"),
	pie = r.piechart(55, 70, 50, pie_data2, { legend: ['ja ', 'nej', 'inget svar'], legendpos: "east", colors: ['#6d01c6', '#575555', '#972323' ]});

	pie.hover(function () {
		this.sector.stop();
		this.sector.scale(1.1, 1.1, this.cx, this.cy);

		if (this.label) {
			this.label[0].stop();
			this.label[0].attr({ r: 7.5 });
			this.label[1].attr({ "font-weight": 800 });
		}
	}, function () {
		this.sector.animate({ transform: 's1 1 ' + this.cx + ' ' + this.cy }, 500, "bounce");

		if (this.label) {
			this.label[0].animate({ r: 5 }, 500, "bounce");
			this.label[1].attr({ "font-weight": 400 });
		}
	});
}