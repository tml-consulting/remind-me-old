// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery/jquery-1.9.1.min
//require jquery/jquery-ui-1.10.2.custom.min

//= require jquery/jquery-ui-1.10.3.custom.min



//= require bootstrap

function _m(){
	return $("meta[name=csrf-token]").attr('content');
}

$('.ajax-typeahead').typeahead({
	appData: [],
    source: function(query, process) {
        return $.ajax({
            url: $(this)[0].$element[0].dataset.link,
            type: 'get',
            data: {query: query},
            dataType: 'json',
            success: function(json) {

     			var found_objects = [];
     			appData 		  = {};
     			for(var i = 0; i < json.length; i++ ){
     				name = json[i]['name'];
     				id   = json[i]['id'] ;

     				found_objects.push( name);
     				appData[name] = id ;
     			}	

     			process(found_objects);
            }
        });
    },
    updater: function(obj) {
    	console.log(appData[obj]);
    	window.location.href = "/patients/" + appData[obj];
    }

});

