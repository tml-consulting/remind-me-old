//= require xdate

//= require mustache
//= require libs/underscore
//= require jquery.mustache
//= require libs/jquery.tokeninput
//= require fullcalendar.min
//= require gcal.js

$empty = function(){}

$.Mustache.addFromDom();

$.ajaxSetup({
  headers: {
    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
  }
});

function make_calendar(){
	window.calendar = {
		event:{
			title: function(event){
				return event.title
			}, 
			content: function(event){	
				console.log("Not changed for new layout!")
			}			
		},
		modals: {
			edit: $("#event-edit-modal"), 
			recurring_edit: $("#recurring-event-popin"),
			options: {
				backdrop: true
			}
		},
		callbacks: {
			save_btn: function(e, event, form){
				e.preventDefault();
				// Pass a callback to save, so we don't tie the save-function to 
				// be modal only!

				var errors = "";
				// check that required fields are filled 
				if(form.find('.name-field').val() == "" ){
					errors += "Du måste fylla i ett händelsenamn <br />";
				}

				if(form.find('.start-hour-field').val() == "" ){
					errors += "Du måste fylla i en start timme <br />";
				}
				if(form.find('.start-minute-field').val() == "" ) {
					errors += "Du måste fylla i en start minut <br />";
				}

				if(form.find('.end-hour-field').val() == "" ){
					errors += "Du måste fylla i en slut timme <br />";
				}
				if(form.find('.end-minute-field').val() == "" ) {
					errors += "Du måste fylla i en slut minut <br />";
				}

				var startTime = form.find('.start-hour-field').val()+":"+form.find('.start-minute-field').val(); 
				var endTime = form.find('.end-hour-field').val()+":"+form.find('.end-minute-field').val(); 

				if(!validateTime(startTime)){
					errors += "Din starttid verkar inte vara en giltig tid? <br />"
				}

				if(!validateTime(endTime)){
					errors += "Din sluttid verkar inte vara en giltig tid? <br />"					
				}


				if(errors != ""){
					form.find('.form-errors').remove();

					form.prepend("<div class='form-errors text-error'><b>Kunde inte spara event:</b><br />" + errors + "</div>");
					return false;
				} else {
					form.find('.form-errors').remove();
				}


				window.calendar.save(event, form, function(success, event){
					// The callback receives an updated event from save - so let 
					// fullCalendar know about the updates
					window.calendar.fullcalendar.fullCalendar('updateEvent', event);
					// Hide the modal on a complete save
					window.calendar.modals.edit.modal('hide');
				}); 
			}, 
			delete_btn: function(e, event, form){
				e.preventDefault();
				window.calendar.delete(event, function(success, event){
					if(success){
						window.calendar.modals.edit.modal('hide');						
					}
				});
			}
		},
		// Display the popover when clicking an event, except if it's a new event!
		show: function(event){
			var event = window.calendar._munge(event); 
			var _e = $(window.calendar._element_for_event(event));
			console.log(event);
			event.formattedStartDate = window.calendar.fullcalendar.fullCalendar('formatDate', event.start, "ddd, 'den' d MMMM, HH:mm - ")+
			window.calendar.fullcalendar.fullCalendar('formatDate', event.end, 'HH:mm');
			if(!_e.data('has-popover')){
				console.log(event);
				var _d = event.start.getDay(); 
				if(event.allDay){
					var dir = 'bottom';
				}else if(_d > 4 || _d == 0){
					var dir = 'left'; 
				}else{
					var dir = 'right'; 
				}
				_e.popover('destroy').popover({
					html: true, 
					trigger: 'manual',
					placement: dir,
					content: function(){
						var _c = $($.Mustache.render('event-show-tmpl', event));
						_c.find('.popover-edit-link').bind('click', function(){
								_e.popover('hide'); 
								window.calendar.edit(event);								
						});
						return _c; 
					}
				})
				_e.data('has-popover', true);
			}
			_e.popover('toggle');
		},
		check_edit_style_for_recurring_event: function(event, callback){
			if(event.recurring && event.recurring != 'n'){
				var _e = $(window.calendar._element_for_event(event));
				_e.popover('destroy');
				_e.popover({
					html: true, 
					trigger: 'manual',
					container: 'body',
					content: function(){
						var _c = $($.Mustache.render('recurring-edit-tmpl', event));
						_c.find('.all-events').bind('click', function(e){
							$(e.currentTarget).parents('.popover').remove();
							callback(true);
						});
						_c.find('.single-event').bind('click', function(e){
							$(e.currentTarget).parents('.popover').remove();
							callback(false);
						});
						return _c; 
					}
				});
				window.current_popover = _e.popover('show');

			}else{
				callback(false);
			}
		},
		edit: function(event){
			// Munge the event, see the method _munge and http://mustache.github.io/mustache.5.html#lambdas for details
			var event = window.calendar._munge(event);
			var edit_function = function(edit_all_recurring){
				event.edit_all_recurring = edit_all_recurring;
				event.reminders_empty = (event.reminders || []).length == 0;
				// Fetch a new form from Mustache, convert it to a jQuery object				
				var form = $($.Mustache.render('event-tmpl', event));

				// Hook up events
				form.find('.save').bind('click', function(e){
					var form = $(e.currentTarget).parents('.modal-content').find('.event-modal-form');
					window.calendar.callbacks.save_btn(e, event, form);
				}); 
				form.find('.delete').bind('click', function(e){
					window.calendar.callbacks.delete_btn(e, event, form);
				});
				// In order to keep reminder-data up to date, notify the 
				// reminder management about any changes to the start time, 
				// so it can update in real-time
				form.find('.start-time-field').on('change', function(e){
					var form = $(e.currentTarget).parents('.modal-body');
					var new_date = event.start.clone(); 
					new_date.setHours(form.find('.start-hour-field').val());
					new_date.setMinutes(form.find('.start-minute-field').val());

					window.calendar._refresh_reminders(new_date, form);
				});

				window.calendar._setup_reminder_editing(event, form);

				// Set the proper content into the modal
				window.calendar.modals.edit.find('.modal-content').html(form);
				// Make sure we remove the event from the calendar if it's not saved
				// when the user closes the modal.

				// Hook up events to the reminders so that the interaction works 
				// as expected

				window.calendar._refresh_reminders(event.start, window.calendar.modals.edit.find('.modal-content'));


				window.calendar.modals.edit.one('hide', function(){
					if(!event.id){
						window.calendar.delete(event, $empty);
					}
					// Perhaps, we should remove all edit-event handlers here and see what happens
//					form.find('.person-field').tokenInput('destroy');
				});

				// Prepare the modal for viewing
				window.calendar.modals.edit.one('shown', function(){
					form.find('.name-field').focus();
					$('.person-field').tokenInput('/calendar/persons', {
						theme: 'mac', 
						hintText: 'Skriv för att söka', 
						tokenValue: 'name',
						noResultsText: 'Inget resultat, skriv namnet och tryck <enter>', 
						searchingText: 'Söker...',
						allowFreeTagging: true,
						prePopulate: event.participants
					});
				});
				// Trigger the modal, using the default options
				window.calendar.modals.edit.modal(window.calendar.modals.options);	
			}		
			// Wrap the actual edit in an edit-style check!
			window.calendar.check_edit_style_for_recurring_event(event, edit_function)
		},
		save: function(event, form, callback){
			var payload = window.calendar._construct_payload(event, form);
			$.ajax({
				url: calendar_base_url, 
				data: payload, 
				type: 'POST', 
				dataType: 'json', 
				success: function(data){
					// Convert date data from upstream into XDate to properly manage timezone.
					var t = new XDate(data.start); 
					var e = new XDate(data.end);
					// Remove the data-format, since it's delivered in a format non-compatible 
					// with straight editing of events
					delete(data.start); 
					delete(data.end);
					// Set the proper dates using native Date-objects constructed from XDate
					data.start 	= new Date(t.getTime()*1000);
					data.end 		= new Date(e.getTime()*1000)
					data.new 		= false
					// Update the rest of the data
					$.extend(event, data);
					if(event.recurring){
						window.calendar.fullcalendar.fullCalendar('refetchEvents');
					}
					// Execute callback
					callback(true, event)
				}, 
				error: function(){
					console.log(arguments);
					callback(false, event)
				}
			});
		},
		delete: function(event, callback){			
			if(event.id){
				var _e = window.calendar._element_for_event(event);
				// Update this...
				var _el = (event.allDay ? _e.find('.fc-event-inner') : _e.find('.fc-event-bg'));
				_el.addClass('ui-progress');			
				$.ajax({
					url: calendar_base_url, 
					data: {event: {event_id: event.id, edit_all_recurring: event.edit_all_recurring}, '_method':'delete'}, 						
					type: 'POST', 
					success: function(json){
						window.calendar.fullcalendar.fullCalendar('removeEvents', event._id);
						if(event.recurring){
							window.calendar.fullcalendar.fullCalendar('refetchEvents');
						}
						callback(true, event)
					}, 
					error: function(){
						alert("this must be displayed in some kind of nice manner");					
					}
				});	
			}else{
				window.calendar.fullcalendar.fullCalendar('removeEvents', event._id);
				callback(true);
			}
		},
		_setUpEventListnersOnPopin: function(event){
			console.log("called setup event listeners");
		},
		_element_for_event: function(event){
			return window.calendar.fullcalendar.fullCalendar('getView').elementByEventID(event._id)[0];
		},
		_construct_payload: function(event, form){
			if(form){
				// Required to work on clones to prevent multi-writing of time-offsets, which will - in turn - 
				// cause a very weird bug!
				var start_clone = event.start.clone().setHours(form.find('.start-hour-field').val())
				.setMinutes(form.find('.start-minute-field').val());

				var end_clone = event.end.clone().setHours(form.find('.end-hour-field').val())
				.setMinutes(form.find('.end-minute-field').val());

				var reminders = []; 
				$('.reminder-edit-row').each(function(i, el){
					// This data is stored on the element in the set_reminder-function.
					var reminder_data = $(el).data('reminder-data'); 
					// We explicitly construct the hash passed here, since reminder_data contains 
					// more keys 
					if(reminder_data){						
						reminders.push({exponent: reminder_data.exponent, multiplier: reminder_data.multiplier});
					}
				});
				if(reminders.length == 0){
					reminders = null;
				}
				return {
					event:{
						event_id: event.id, 
						name: $('.name-field').val(), 
						start: start_clone.toISOString(), 
						end: end_clone.toISOString(), 
						place: $('.place-field').val(), 
						participant_list: $('.person-field').val(),
						all_day: event.allDay, 
						reminder_list: reminders, 
						recurrence_key: $('.recurrence-key').val(), 
						edit_all_recurring: event.edit_all_recurring
				}}
			}else{
				return {event:{
					event_id: event.id, 
					start: event.start.toISOString(), 
					end: event.end.toISOString(),
					all_day: event.allDay, 
					edit_all_recurring: event.edit_all_recurring
				}}
			}
		},
		_munge: function(event){
			$.extend(event, {
				start: new XDate(event.start), 
				end: new XDate(event.end),
				start_hh: function(){ return event.start.toString('HH') },
				start_mm: function(){ return event.start.toString('mm') },
				end_hh: function(){ return event.end.toString('HH') },
				end_mm: function(){ return event.end.toString('mm') }, 
				event_name: function(){ return (event.new ? 'Skapa event' : 'Editera ' + event.title)}
			});
			return event;
		}, 
		_setup_reminder_editing: function(event, form){
			// This should be rewritten, there's no reason this should unset and reset the 
			// event-binders everytime we open the edit-modal.
			$(document).off('click.add-reminder').on('click.add-reminder', '.add-reminder', function(e){
				window.calendar.render_reminder_row(e, event, true);
			});
			$(document).off('click.remove-reminder').on('click.remove-reminder', '.remove-reminder', function(e){
				window.calendar.remove_reminder(e, event, true);
			});
			form.find('.reminder-edit-row').each(function(i, row){
				var row = $(row);
				window.calendar.set_reminder(null, row, event.start);
				window.calendar._hook_reminder_events(row, event.start);
			});
		}, 
		render_reminder_row: function(click_event, calendar_event, edit_mode){
			// Here - calculate a probable time for the new reminder, to render
			// into the form to start with.
			var row = $($.Mustache.render('reminder-row-tmpl', {})); 
			var v = window.calendar.modals.edit;
			if(v.find('.placeholder-row').length != 1){
				$(click_event.currentTarget).parents('tr').after(row);
			}else{
				v.find('.reminder-list-body').html("").append(row);
			}	
			window.calendar._hook_reminder_events(row, calendar_event.start);
		}, 
		_hook_reminder_events: function(row, time){
			row.find('input:first').focus().off('.reminder').on('change.reminder', function(e){
				window.calendar.set_reminder(e, row, time);
			});
			row.find('select').off('.reminder').on('change.reminder', function(e){
				window.calendar.set_reminder(e, row, time);				
			});	
		},
		set_reminder: function(change_event, row, event_start_time){
			var exponent 		= row.find('.reminder-time-input').val(); 
			var multiplier 	= row.find('.reminder-input-select').val(); 
			var _clone = event_start_time.clone(); 
			switch(multiplier){
				case "m": 
					_clone.addMinutes(-Number(exponent)); 
					break;
				case "h": 
					_clone.addHours(-Number(exponent)); 				
					break; 
				case "d": 
					_clone.addDays(-Number(exponent)); 
					break;
			}
			row.find('.reminder-time-hint').addClass('valid-time').html("( "+_clone.toString('HH:mm')+" )")
			row.data('reminder-data', {exponent: exponent, multiplier: multiplier, remind_at_epoch: _clone.getTime()});

			$('.reminder-edit-row').sortElements(function(a,b){
				var data_a = $(a).data('reminder-data'); 
				var data_b = $(b).data('reminder-data');
				if(data_a && data_b){
					return data_a.remind_at_epoch < data_b.remind_at_epoch ? 1 : -1
				}else{
					return 0;
				}
			});

		},
		remove_reminder: function(click_event, calendar_event, edit_mode){
			var row = $(click_event.currentTarget).parents('tr'); 
			row.remove();
			var table_body = $('.reminder-list-body');
			if(table_body.find('tr').length == 0){
				table_body.html($.Mustache.render('reminder-placeholder-row-tmpl', {}));
			}
		},
		_refresh_reminders: function(start_time, form){
			$(".current-start-time").html(start_time.toString('HH:mm'));
			form.find('.reminder-edit-row').each(function(i, row){
				var row = $(row);
				window.calendar.set_reminder(null, row, start_time);
				window.calendar._hook_reminder_events(row, start_time);
			});
		},
		// Validate that this is a valid time-string in format
		_time_validator: function(time){
			if(!time.match(/^\d{1,2}$/)){return false}
			return true;
		}
	};
	
	$(window).bind('keyup', function(e){
		if(e.which == 27){
			window.calendar.popover.hideAll();
		}
	});
	
	window.calendar.fullcalendar = $('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'agendaWeek,agendaDay'
		},
  		eventSources: [
        {
	        url: window.calendar_base_url + "/events", 
            type: 'GET',
            error: function() {
                alert('there was an error while fetching events!');
            }
        }
    ],
		dayClick: function(date, allDay, jsEvent, view) {
			if(date < new Date()){
				return;
			}
			window.calendar.fullcalendar.fullCalendar('renderEvent', {
				title:'', 
				start: new XDate(date),
				end: new XDate(date).addHours(2), 
				allDay: allDay, 
				new: true
			});
		},	
		// Called as callback from fullCalendar after rendering each event.
		eventAfterRender: function(event, element, view){			
			// If this event hasn't been saved before, the user has just clicked in 
			// the calendar to add a new event, so let's open the edit-box as expected.
			if(event.new){
				window.calendar.edit(event);
			}
		},
		eventDragStart: function(event, jsEvent, view){

		},
		eventDrop: function(event, jsEvent, view){
			if(event.id){
				window.calendar.check_edit_style_for_recurring_event(event, function(edit_all_recurring){				
					event.edit_all_recurring = edit_all_recurring;
					window.calendar.save(event, null, $empty);				
				});
			}
		},
		eventResize: function(event, jsEvent, view){
			if(event.id){
				window.calendar.check_edit_style_for_recurring_event(event, function(edit_all_recurring){		
					event.edit_all_recurring = edit_all_recurring;
					window.calendar.save(event, null, $empty);							
				}); 
			}
		},
		eventClick: function(event, jsEvent, view){
			if(event.editable){
				if(event.new){
					event.start = new XDate(event.start); 
					event.end = new XDate(event.end); 
					window.calendar.edit(event);									
				}else{
					window.calendar.show(event);					
				}
			}else{
				window.calendar.show(event);
			}
		},
		editable: true,
		defaultView: 'agendaWeek',
		eventColor: '#6f02c9',
		firstDay: 1,
		allDayText: 'hela dagen',
		columnFormat: {
			month: 'ddd', 
			week: 'ddd d/M', 
			day: 'dddd'
		},
		timeFormat: {agenda: 'H:mm{ - H:mm}'},
		monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Maj', 'Jun','Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dec'],
		monthNames: ['Januari', 'Februari', 'Mars', 'April', 'Maj', 'Juni', 'Juli', 'Augusti', 'September', 'Oktober', 'November', 'December'],
		dayNamesShort: ['Sön', 'Mån', 'Tis', 'Ons', 'Tor', 'Fre', 'Lör'],
		dayNames: ['Söndag', 'Måndag', 'Tisdag', 'Onsdag', 'Torsdag', 'Fredag', 'Lördag'],
		buttonText:{
			prev:     '&nbsp;&#9668;&nbsp;',  // left triangle
			next:     '&nbsp;&#9658;&nbsp;',  // right triangle
			prevYear: '&nbsp;&lt;&lt;&nbsp;', // <<
			nextYear: '&nbsp;&gt;&gt;&nbsp;', // >>
			today:    'idag',
			month:    'månad',
			week:     'vecka',
			day:      'dag'
		},
		axisFormat: 'HH:mm',
		firstHour: 6, // use minTime if we want to restrict users to start at 6 
	});	
}


window.google = {
	success: function(){
		alert("account connected");
	}, 
	connection_failed: function(){
		alert("couldn't connect accounts");
	},
	not_logged_in: function(){
		alert("log in again");
	}
}


$('.google-connect-link').click(function(e){
	// Calculate proper offset from left for the window
	var left_offset = ($(window).width()-600)/2;
	window.open("/auth/google/authorize", '', 'width=600,height=500,top=150,left='+left_offset);
	return false;
});


function validateTime(value){
	console.log(value);
	return value.match(/^([01]?[0-9]|2[0-3]):[0-5][0-9]$/);
}


jQuery.fn.sortElements = (function(){
 
    var sort = [].sort;
 
    return function(comparator, getSortable) {
 
        getSortable = getSortable || function(){return this;};
 
        var placements = this.map(function(){
 
            var sortElement = getSortable.call(this),
                parentNode = sortElement.parentNode,
 
                // Since the element itself will change position, we have
                // to have some way of storing its original position in
                // the DOM. The easiest way is to have a 'flag' node:
                nextSibling = parentNode.insertBefore(
                    document.createTextNode(''),
                    sortElement.nextSibling
                );
 
            return function() {
 
                if (parentNode === this) {
                    throw new Error(
                        "You can't sort elements if any one is a descendant of another."
                    );
                }
 
                // Insert before flag:
                parentNode.insertBefore(this, nextSibling);
                // Remove flag:
                parentNode.removeChild(nextSibling);
 
            };
 
        });
 
        return sort.call(this, comparator).each(function(i){
            placements[i].call(getSortable.call(this));
        });
 
    };
 
})();