//= require xdate
//= require mustache
//= require libs/underscore
//= require jquery.mustache

$.Mustache.addFromDom();

$.ajaxSetup({
  headers: {
    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
  }
});

$(function(){
	setup_auth_link();
	setup_reminder_editing();
});

function oauth_success(){
	$('#google-account-section').load('/settings/refresh_google_status'); 
}

function oauth_error(){
	console.log("error - handle this in the general, error-message way"); 
}

function setup_auth_link(){
	$(".auth-link").bind('click', function(e){
		e.preventDefault(); 
		var _t = $(this); 
		if(!window.oauth_window){
			window.oauth_window = window.open(_t.attr('href'), '_blank', 'height=650,width=500');			
		}else{
			window.oauth_window.focus(); 
		}
	});
}


// I will give this a refactoring once all functionality is in place! .daniel

function add_reminder(e){
	var row = $.Mustache.render('reminder-edit-row', {}); 
	var v = $(".reminder-edit-table");
	if(v.find('.placeholder-row').length != 1){
		$(e.currentTarget).parents('tr').after(row);
	}else{
		v.html("").append(row);
	}	
}

function remove_reminder(e){
	$(e.currentTarget).parents('tr').remove();
	var table_body = $('.reminder-edit-table');
	if(table_body.find('tr').length == 0){
		table_body.html($.Mustache.render('reminder-placeholder-row-tmpl', {}));
	}
}

function set_reminder(row){
	var exponent 					= row.find('.reminder-time-input').val(); 
	var multiplier 				= row.find('.reminder-input-select').val(); 
	row.data('reminder-data', {exponent: exponent, multiplier: multiplier});
}

function resort_reminders(){
	var _comparison_date = new XDate();
	$('.reminder-edit-table tr').sortElements(function(a,b){
		var data_a = $(a).data('reminder-data'); 
		var data_b = $(b).data('reminder-data');
		return calculate_date_offset(data_a, _comparison_date).getTime() < calculate_date_offset(data_b, _comparison_date).getTime() ? 1 : -1
	});
}

function calculate_date_offset(definition, base_date){
	var _t = base_date.clone(); 
	switch(definition.multiplier){
		case "m": 
			_t.addMinutes(-Number(definition.exponent)); 
			break;
		case "h": 
			_t.addHours(-Number(definition.exponent)); 				
			break; 
		case "d": 
			_t.addDays(-Number(definition.exponent)); 
			break;
	}
	return _t; 
}

function save_reminders(){
	var reminders = []; 
	$('.reminder-edit-row').each(function(i, el){
		var reminder_data = $(el).data('reminder-data'); 
		reminders.push({exponent: reminder_data.exponent, multiplier: reminder_data.multiplier});
	});

	$.ajax({
		url: '/settings/reminders', 
		type: 'post', 
		dataType: 'html',
		data: {
			reminders: reminders, 
			patient_id: $(".save-reminders-btn").attr('data-edit-for')
		}, 
		success: function(data){
			$(".default-reminder-list tbody").html(data);
			$("#reminder-edit-modal").modal('hide');
		}, 
		error: function(){

		}
	});
}

function setup_reminder_editing(){
	$(document.body).on('click', '.reminder-edit-table .add-reminder', add_reminder); 
	$(document.body).on('click', '.reminder-edit-table .remove-reminder', remove_reminder); 
	$(document.body).on('change', '.reminder-input-select', function(e){
		var row = $(e.currentTarget).parents('tr'); 
		set_reminder(row);
		resort_reminders();
	}); 
	$(document.body).on('change', '.reminder-time-input', function(e){
		var row = $(e.currentTarget).parents('tr'); 
		set_reminder(row);
		resort_reminders();
	}); 
	$(document.body).on('click', '.save-reminders-btn', function(e){
		save_reminders();
	})

	$(".edit-reminders").on('click', function(){
		var _t = $(this);
		// Render reminder-rows here
		var data = {reminders: []}; 
		$(".default-reminder-list tr").each(function(i, el){
			var _d = {exponent: el.getAttribute('data-exponent'), multiplier: el.getAttribute('data-multiplier')};
			// This seemingly weird construct is to allow straight rendering via Mustache.
			switch(_d.multiplier){
				case 'm':
					_d.is_minute = true; 
					break;
				case 'h': 
					_d.is_hour = true; 
					break; 
				case 'd':
					_d.is_day = true;
					break;
			}
			data.reminders.push(_d); 
		});
		if(data.reminders.length == 0){
			data.no_reminders = true			
		}
		$(".reminder-edit-table").html($.Mustache.render('reminder-row-section', data));
		$(".reminder-edit-table tr").each(function(i, el){set_reminder($(el));})
		resort_reminders();
		$(_t.attr('href')).on('shown', function(){

		}).modal();
	});
}

// Move this to a central plugin-definition instead! .daniel
jQuery.fn.sortElements = (function(){
 
    var sort = [].sort;
 
    return function(comparator, getSortable) {
 
        getSortable = getSortable || function(){return this;};
 
        var placements = this.map(function(){
 
            var sortElement = getSortable.call(this),
                parentNode = sortElement.parentNode,
 
                // Since the element itself will change position, we have
                // to have some way of storing its original position in
                // the DOM. The easiest way is to have a 'flag' node:
                nextSibling = parentNode.insertBefore(
                    document.createTextNode(''),
                    sortElement.nextSibling
                );
 
            return function() {
 
                if (parentNode === this) {
                    throw new Error(
                        "You can't sort elements if any one is a descendant of another."
                    );
                }
 
                // Insert before flag:
                parentNode.insertBefore(this, nextSibling);
                // Remove flag:
                parentNode.removeChild(nextSibling);
 
            };
 
        });
 
        return sort.call(this, comparator).each(function(i){
            placements[i].call(getSortable.call(this));
        });
 
    };
 
})();