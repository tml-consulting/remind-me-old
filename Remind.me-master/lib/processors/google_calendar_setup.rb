class GoogleCalendarSetup
	@queue = :google_calendar_setup

	def self.perform(account_id)
		google_account			= GoogleAccount.find_by_id(account_id)
		client 							= google_account.api_client
		calendar 						= client.discovered_api('calendar', 'v3')

		existing_calendars 					= client.execute(calendar.calendar_list.list)
		existing_remindme_calendar	= nil
		existing_calendars.data.items.each do |calendar|
			if calendar.summary.match(/remind/i) || calendar.id == google_account.calendar_id
				existing_remindme_calendar = calendar
			end
		end

		if existing_remindme_calendar
			google_account.calendar_id = existing_remindme_calendar.id
			google_account.save
		else
			new_cal = calendar.schemas['Calendar'].new
			# Move this data to configatron!
			new_cal.summary = "Remind.me"
			new_cal.description = "Synkroniserad kalender med remind.me"
			new_cal.location = "Sweden"
			new_cal.time_zone = "CET"
			result = client.execute(
				:api_method => calendar.calendars.insert, 
				:body => JSON.dump(new_cal), 
				:headers => {'Content-Type' => 'application/json'}
			)
			if result.success? 
				google_account.calendar_id = result.data.id
				google_account.save
				google_account.setup!
			else
				google_account.setup_failed!
			end
		end

	end

end