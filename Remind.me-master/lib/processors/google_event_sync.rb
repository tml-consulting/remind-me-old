class GoogleEventSync
	@queue = :google_event_sync

	def self.perform(event_id)
		event 			= Event.find(event_id)
		# If this user is not connected with Google, ignore this event and return!
		return unless event.user.has_google_account?
		account 						= event.user.google_account
		client 							= account.api_client
		calendar 						= client.discovered_api('calendar', 'v3')

		# Construct Google API Resource instance
		google_event = calendar.schemas['Event'].new
		google_event.summary 			= event.name
		google_event.description 	= (event.description || "")
		# The explicit matching nil-attribute is required since we're using
		# patch-semantics for updates! 
		if event.all_day? 
			google_event.start 				= {
				:date => event.start.time.strftime("%Y-%m-%d"), 
				:dateTime => nil
			}
			google_event.end 					= {
				:date => event.end.time.strftime("%Y-%m-%d"), 
				:dateTime => nil
			}
		else
			google_event.start 				= {
				:dateTime => event.start.time.strftime("%FT%T%:z"), 
				:date => nil
			}
			google_event.end 					= {
				:dateTime => event.end.time.strftime("%FT%T%:z"), 
				:date => nil
			}
		end

		# If this event exists in GCal, update - otherwise create
		if event.remote_id
			result = client.execute(
				:api_method => calendar.events.patch, 
				:parameters => {
					'calendarId' 	=> account.calendar_id, 
					'eventId'			=> event.remote_id
				},
				:body => JSON.dump(google_event), 
				:headers => {'Content-Type' => 'application/json'}
			)
			puts "Updating:"
			puts result.inspect
		else
			result = client.execute(
				:api_method => calendar.events.insert, 
				:parameters => {'calendarId' => account.calendar_id},
				:body => JSON.dump(google_event), 
				:headers => {'Content-Type' => 'application/json'}
			)
			event.update_attributes(:remote_id => result.data.id)
			puts "Creating:"
			puts result.inspect
		end
	end

end