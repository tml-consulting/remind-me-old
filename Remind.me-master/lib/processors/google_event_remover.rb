class GoogleEventRemover
	@queue = :google_remove_queue

	def self.perform(payload)
		data = JSON.parse(payload)
		user = User.find_by_id(data['user_id'])
		return unless user.has_google_account?
		account 						= user.google_account
		client 							= account.api_client
		calendar 						= client.discovered_api('calendar', 'v3')
		result = client.execute(
			:api_method => calendar.events.delete, 
			:parameters => {'calendarId' => account.calendar_id, 'eventId' => data['remote_id']},
			:headers => {'Content-Type' => 'application/json'}
		)
	end

end