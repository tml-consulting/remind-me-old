#coding:utf-8
require 'sinatra/respond_to'
require (File.dirname(__FILE__)) + "/../../config/environment" unless defined?(Rails)

class GoogleAuthentication < Sinatra::Base

	enable :logging

	configure do 
		disable :protection
		set :environment, :development
		set :views, ->{ File.join(Rails.root,'app', 'views', 'metal') }
	end

	GoogleAuthentication.register Sinatra::RespondTo
	Tilt.register Tilt::ERBTemplate, 'html.erb'

	before do 
		# Load the Rails4 session¨
		env['rack.session'].send(:load_for_read!)
	end

	helpers do 
		# Fetch and setup Google API Client
		def api_client
			@client ||= GoogleAccount.api_client
			@client
		end

		def user
			user_id = env['rack.session']['warden.user.user.key'][0][0]
			@user ||= User.find_by_id(user_id)
			@user
		end
	end

	get '/' do 
		"Index"
		erb :error
	end


	get '/landing' do 
		redirect api_client.authorization.authorization_uri
	end


	get '/callback' do 
		api_client.authorization.code = params[:code]
		result = api_client.authorization.fetch_access_token!
		account = user.google_account.nil? ? user.build_google_account : user.google_account
		account.update_attributes({
			:auth_token 		=> result['access_token'], 
			:refresh_token	=> result['refresh_token'], 
			:expire_at 			=> Time.at(Time.now.to_i + result['expires_in'].to_i)
		})
		erb :success
	end


	get '/disconnect' do 
		user.google_account.destroy 
		redirect back
	end


end