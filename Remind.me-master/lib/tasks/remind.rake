namespace :remindme do 
	desc "Send all reminders due. Meant to be run every 1 minute."
	task :remind => :environment do 
		timestamp = Time.now.advance(:minutes => 1).utc
		log_file 	= File.new(File.join(Rails.root, 'log', 'reminders.log'), 'a')
		begin
			status = Reminder.send_reminders(timestamp)
		rescue Exception => e
			log_file.puts %{
				Reminder-run at #{timestamp.to_s} failed: 
				#{e.message}
				#{e.backtrace}

				-----
			}
		else
			log_file.puts %{
				Reminder-run at #{timestamp.to_s} succeeded properly: 
				#{status.inspect}

				-----
			}
		ensure
			log_file.close
		end
	end
end