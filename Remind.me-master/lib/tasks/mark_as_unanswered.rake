namespace :event do 
	desc "Expire events that does not have an answer within 12 hours"
	task :expire => :environment do
		Event.expire!(12.hours.ago)
	end
end