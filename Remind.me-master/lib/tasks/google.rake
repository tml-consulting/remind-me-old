namespace "google" do 

	desc "Fetch new events for all known Google Calendars"
	task :sync => :environment do 

		google_accounts = GoogleAccount.all 

		google_accounts.each do |account|
			# Fetch a valid api client and a valid schema definition
			client 							= account.api_client
			calendar 						= client.discovered_api('calendar', 'v3')

			if account.last_event_sync.nil? 
				params = {
					'updatedMin' => (Time.now - 2.weeks).strftime("%FT%T%:z")
				}
			else
				params = {
					'updatedMin' => account.last_event_sync.strftime("%FT%T%:z")				
				}
			end
			# Make sure we fetch events from the correct calendar, and we're not interested
			# in deleted events.
			params['calendarId'] = account.calendar_id
			result = client.execute(:api_method => calendar.events.list, :parameters => params)

			result.data.items.each do |remote_event|
				event = Event.find_by_remote_id(remote_event.id)
				if event && remote_event.status == 'cancelled'
					event.destroy
				elsif event && remote_event.updated > event.updated_at
					event.update_with_google_event(remote_event)
					event.save
				elsif remote_event.status != "cancelled"
					event = account.user.events.new					
					event.update_with_google_event(remote_event)
					event.save
				end
			end

			account.update_attribute(:last_event_sync, Time.now)
		end
	end

end