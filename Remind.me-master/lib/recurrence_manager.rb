# This class provides date calculations for managing recurring times. Right now, 
# it uses a brute-force approach which, in some cases, could be optimized. If this 
# is the case - first look at workdays_until/weekends_until, since it looks at every
# day between two dates and checks which part of the week it represents. .daniel
class RecurrenceManager

	def self.plot(start_time, step, time_scale)
		time = start_time.advance(time_scale)
		case step
			when [:daily]				
				dates = daily_dates_until(start_time, time)
			when [:weekly]
				dates = weekly_dates_until(start_time, time)
			when [:workdays]
				dates = workdays_until(start_time, time)
			else
				dates = []
		end
		dates
	end

	private

	def self.daily_dates_until(start, end_time)
		dates = []
		while start < end_time
			dates << start
			start += 1.day
		end
		dates
	end

	def self.workdays_until(start, end_time)	
		dates = []
		while start < end_time
			dates << start if workdays_of_week.include?(start.wday)
			start += 1.day
		end
		dates
	end

	def self.weekly_dates_until(start, end_time)
		dates = []
		while start < end_time
			dates << start
			start += 1.week
		end
		dates
	end

	def self.workdays_of_week(workdays = true)
		if workdays
			[1,2,3,4,5]
		else
			[0, 6]
		end
	end

end