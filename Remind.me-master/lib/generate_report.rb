module GenerateReport

    class ReportComparison
        attr_accessor :answer_freq_trend, :answer_speed_trend, :comparison_successfull

        def initialize(current, historical)
            @current        = current
            @historical     = historical

            @answer_freq_trend  = 0
            @answer_speed_trend = 0
            @comparison_successfull = analyse

        end

        private 

        def analyse 
            return false if @current.events.empty? || @historical.events.empty? 
            if @current.num_no_answers.eql?(0)
                current_score = 0
            else
                current_score       = (@current.num_answered / (@current.num_no_answers )) 
            end
            historical_score    = (@historical.num_answered.nil? ||  @historical.num_no_answers.nil? || @historical.num_no_answers == 0 ) ? @historical.num_answered : (@historical.num_answered / (@historical.num_no_answers))
            @answer_freq_trend  = current_score - historical_score
            @answer_speed_trend = @current.response_average - @historical.response_average
            return true  
        end
    end

	class Report
	   # include ActiveModel::Validations
	   # include ActiveModel::Conversion
	   # extend ActiveModel::Naming
    	attr_accessor :events, :num_yes_answers, :num_declined_answers, :num_no_answers, :response_average, :total_num_reminders, :avg_num_reminders, :num_events, :num_answered
    	attr_accessor :created_by_patient, :created_by_other, :start, :stop, :users
    	def initialize(start, stop, users)
    		@start = start
    		@stop = stop
    		@users = users

    		@users = [@users] unless users.is_a?(Array)
    		generate_report()
            @users = User.find(@users)

    	end

		private


		def calculate_statistics
    		@num_yes_answers 		= 0
    		@num_declined_answers 	= 0
    		@num_no_answers 		= 0
    		@num_answered 			= 0
    		@total_num_reminders 	= 0
    		@created_by_patient		= 0 
    		@created_by_other		= 0
    		time                    = 0

    		@events.each do |e|
    			@num_yes_answers += 1        if e.reminder_status == 1
    			@num_declined_answers += 1   if e.reminder_status == 2
    			@num_no_answers += 1         if e.reminder_status == 3
    			time += e.response_time_in_sec unless e.response_time_in_sec.nil?
    			@total_num_reminders += e.num_reminders_required unless e.num_reminders_required.nil?
                if e.creator.nil? || e.creator == e.user
                    @created_by_patient += 1
                else 
                    @created_by_other += 1
                end
    		end
    		unless @num_events == 0 || @num_events.nil?
	    		@response_average 	=  time / @num_events
    			@avg_num_reminders 	= @total_num_reminders / @num_events
    		end

    		@num_answered 		= @num_yes_answers + @num_declined_answers

		end

    	 
    	def generate_report 
    		finder_sql = "FROM events as e WHERE start >= ? AND start <= ? AND deleted_at is null AND reminder_status IS NOT NULL AND e.user_id IN (#{@users.join(',')})"
    		@events 	= Event.find_by_sql(["SELECT e.* " + finder_sql + " ORDER BY start", @start, @stop])
    		@num_events = @events.count
    		calculate_statistics
    	end

	end

    def self.generate_report(start, stop, users_def)
        return Report.new(start, stop, users_def)
    end

	def self.generate_comparison(current, historical)
		return ReportComparison.new(current, historical)
	end
end # End of generate report