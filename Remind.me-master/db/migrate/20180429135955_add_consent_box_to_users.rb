class AddConsentBoxToUsers < ActiveRecord::Migration
  def change
    add_column :users, :processing_consent_given, :boolean, :default => false
  end
end
