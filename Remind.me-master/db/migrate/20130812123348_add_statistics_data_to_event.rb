class AddStatisticsDataToEvent < ActiveRecord::Migration
 	def change
  		add_column :events, :num_reminders_required, :integer
  		add_column :events, :response_time_in_sec, :integer
  		add_column :events, :reminder_status, :integer
	end
end
