class AddSupportForRecurrence < ActiveRecord::Migration
  def change
  	add_column :events, :recurrence, :integer
  	add_column :events, :recurrence_id, :integer
  end
end
