class AddWorkflowToGoogleAccounts < ActiveRecord::Migration
  def change
  	add_column :google_accounts, :workflow_state, :string
  end
end
