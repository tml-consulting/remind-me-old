class AddAdditionalTimestampsToUser < ActiveRecord::Migration
  def change
  	  	add_column :users, :last_login_at, :datetime
  	  	add_column :users, :last_session_at, :datetime
  	
  end
end
