class CreateTextMessages < ActiveRecord::Migration
  def change
    create_table :text_messages do |t|
    	t.boolean 		:incoming, :default => false
    	t.string 			:sender
    	t.text 				:body
    	t.datetime		:sent_at
    	t.string			:operator_identifier
    	t.string			:custom_message_id
    	t.integer			:reminder_id
    	t.integer			:user_id
    	t.string			:receiver
    	t.string			:workflow_state
      t.timestamps
    end
  end
end
