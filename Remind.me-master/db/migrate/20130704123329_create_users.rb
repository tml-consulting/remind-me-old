class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
	    t.string   "login",                       :limit => 40
	    t.string   "name",                        :limit => 100, :default => ""

	    t.integer  "role"
	    t.string   "phone"

	    t.string   "type"
	    t.integer  "terapeut_id"

	    t.string   "workflow_state"
	    t.integer  "default_number_of_reminders",                :default => 10
	    t.integer  "default_reminder_offset",                    :default => 120
    	t.timestamps
 	end
  end
end