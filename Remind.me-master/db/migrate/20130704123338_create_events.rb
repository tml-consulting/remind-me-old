class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
    	t.integer  "user_id"
	    t.string   "name"
	    t.text     "description"
	    t.datetime "start"
	    t.datetime "end"
	    t.string   "workflow_state"
	    t.boolean  "added_in_session"
	    t.string   "remote_id"
	    t.string   "place"
	    t.boolean  "all_day"
      t.timestamps
    end
  end
end
