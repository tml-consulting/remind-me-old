class CreateResponses < ActiveRecord::Migration
  def change
    create_table :responses do |t|
      t.string :response_text
      t.integer :reminder_id
      t.integer :message_id
      t.timestamps
    end
  end
end
