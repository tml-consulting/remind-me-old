class ChangeRecurrenceIdentifierType < ActiveRecord::Migration
  def change
  	remove_column :events, :recurrence_id
  	add_column :events, :recurrence_identifier, :string
  end
end
