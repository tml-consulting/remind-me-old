class IncreaseLengthOfInvitationToken < ActiveRecord::Migration
  def change
  	change_column :users, :invitation_token, :string, :length => 255
  end
end
