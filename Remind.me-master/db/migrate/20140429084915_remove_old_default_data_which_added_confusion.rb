class RemoveOldDefaultDataWhichAddedConfusion < ActiveRecord::Migration
  def change
  	remove_column :users, :default_number_of_reminders
  	remove_column :users, :default_reminder_offset
  end
end
