class AddExpireFieldToGoogleAccount < ActiveRecord::Migration
  def change
  	add_column :google_accounts, :expire_at, :datetime
  end
end
