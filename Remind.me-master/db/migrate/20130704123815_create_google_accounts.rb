class CreateGoogleAccounts < ActiveRecord::Migration
  def change
    create_table :google_accounts do |t|
	    t.integer  "user_id"
	    t.string   "email"
	    t.string   "auth_token"
	    t.string   "auth_secret"
	    t.string   "calendar_id"
	    t.datetime "last_event_sync"
	    t.datetime "last_data_sync"
    	t.timestamps
    end
  end
end
