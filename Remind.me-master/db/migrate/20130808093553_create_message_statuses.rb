class CreateMessageStatuses < ActiveRecord::Migration
  def change
    create_table :message_statuses do |t|
    	t.integer 	:text_message_id
    	t.string		:operator_status
    	t.datetime	:performed_at
      t.timestamps
    end
  end
end
