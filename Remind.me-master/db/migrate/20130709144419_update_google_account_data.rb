class UpdateGoogleAccountData < ActiveRecord::Migration
  def change
  	remove_column :google_accounts, :email
  	add_column :google_accounts, :refresh_token, :string
  	  	
  end
end
