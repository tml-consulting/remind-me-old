class CreateReminders < ActiveRecord::Migration
  def change
    create_table :reminders do |t|
	    t.string   "workflow_state"
	    t.datetime "remind_at"
	    t.integer  "offset"
	    t.integer  "event_id"
      	t.timestamps
    end
  end
end
