class AddSkipReminders < ActiveRecord::Migration
  def change
  	add_column :events, :skip_reminders, :boolean, :default => false
  end
end
