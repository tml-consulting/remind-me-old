class UpdateRemindersToNewStructure < ActiveRecord::Migration
  def change
  	remove_column :reminders, :offset
  	add_column :reminders, :exponent, :integer
  	add_column :reminders, :multiplier, :string
  end
end
