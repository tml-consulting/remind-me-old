class AddMessageIdentifierToReminder < ActiveRecord::Migration
  def change
  	add_column :reminders, :message_identifier, :string
  end
end
