class FixBugInvitableMigration < ActiveRecord::Migration
  def change
  	change_table :users do |t|
   	 t.datetime :invitation_created_at	
	end
  end
end
