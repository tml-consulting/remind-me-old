class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
   	  t.text :content
   	  t.string :title
   	  t.integer :user_id
   	  t.integer :orgin_id
      t.timestamps
    end
  end
end
