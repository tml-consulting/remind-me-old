class AddRemindersToEvents < ActiveRecord::Migration
  def change
  	add_column :events, :num_reminders, :integer
  	add_column :events, :reminder_offset, :integer
  	
  end
end
