class AddCreatedByIdToEvents < ActiveRecord::Migration
  def change
 	add_column :events, :creator_id, :integer

 	# Update all users to conform to the new standard.
 	# Event.all.each do |e|
 	# 	e.creator_id = e.user_id if e.creator_id.nil?
 	# 	e.save
 	# end

  end
end
