# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180429135955) do

  create_table "events", force: :cascade do |t|
    t.integer  "user_id",                limit: 4
    t.string   "name",                   limit: 255
    t.text     "description",            limit: 65535
    t.datetime "start"
    t.datetime "end"
    t.string   "workflow_state",         limit: 255
    t.boolean  "added_in_session",       limit: 1
    t.string   "remote_id",              limit: 255
    t.string   "place",                  limit: 255
    t.boolean  "all_day",                limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "num_reminders",          limit: 4
    t.integer  "reminder_offset",        limit: 4
    t.integer  "num_reminders_required", limit: 4
    t.integer  "response_time_in_sec",   limit: 4
    t.integer  "reminder_status",        limit: 4
    t.integer  "creator_id",             limit: 4
    t.integer  "recurrence",             limit: 4
    t.string   "recurrence_identifier",  limit: 255
    t.boolean  "skip_reminders",         limit: 1,     default: false
    t.datetime "deleted_at"
  end

  create_table "google_accounts", force: :cascade do |t|
    t.integer  "user_id",         limit: 4
    t.string   "auth_token",      limit: 255
    t.string   "auth_secret",     limit: 255
    t.string   "calendar_id",     limit: 255
    t.datetime "last_event_sync"
    t.datetime "last_data_sync"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "refresh_token",   limit: 255
    t.datetime "expire_at"
    t.string   "workflow_state",  limit: 255
  end

  create_table "message_statuses", force: :cascade do |t|
    t.integer  "text_message_id", limit: 4
    t.string   "operator_status", limit: 255
    t.datetime "performed_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", force: :cascade do |t|
    t.text     "content",     limit: 65535
    t.integer  "user_id",     limit: 4
    t.integer  "reminder_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notes", force: :cascade do |t|
    t.text     "content",    limit: 65535
    t.string   "title",      limit: 255
    t.integer  "user_id",    limit: 4
    t.integer  "orgin_id",   limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "participants", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "participations", force: :cascade do |t|
    t.integer  "event_id",       limit: 4
    t.integer  "participant_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "permissions", force: :cascade do |t|
    t.integer  "user_id",      limit: 4
    t.integer  "access_to_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reminders", force: :cascade do |t|
    t.string   "workflow_state",     limit: 255
    t.datetime "remind_at"
    t.integer  "event_id",           limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "exponent",           limit: 4
    t.string   "multiplier",         limit: 255
    t.integer  "user_id",            limit: 4
    t.string   "message_identifier", limit: 255
  end

  create_table "responses", force: :cascade do |t|
    t.string   "response_text", limit: 255
    t.integer  "reminder_id",   limit: 4
    t.integer  "message_id",    limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "text_messages", force: :cascade do |t|
    t.boolean  "incoming",            limit: 1,     default: false
    t.string   "sender",              limit: 255
    t.text     "body",                limit: 65535
    t.datetime "sent_at"
    t.string   "operator_identifier", limit: 255
    t.string   "custom_message_id",   limit: 255
    t.integer  "reminder_id",         limit: 4
    t.integer  "user_id",             limit: 4
    t.string   "receiver",            limit: 255
    t.string   "workflow_state",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "login",                    limit: 40
    t.string   "name",                     limit: 100, default: ""
    t.integer  "role",                     limit: 4
    t.string   "phone",                    limit: 255
    t.string   "type",                     limit: 255
    t.integer  "terapeut_id",              limit: 4
    t.string   "workflow_state",           limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email",                    limit: 255, default: "",    null: false
    t.string   "encrypted_password",       limit: 255, default: ""
    t.string   "reset_password_token",     limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            limit: 4,   default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",       limit: 255
    t.string   "last_sign_in_ip",          limit: 255
    t.datetime "last_login_at"
    t.datetime "last_session_at"
    t.string   "invitation_token",         limit: 255
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit",         limit: 4
    t.integer  "invited_by_id",            limit: 4
    t.string   "invited_by_type",          limit: 255
    t.datetime "invitation_created_at"
    t.string   "first_name",               limit: 255
    t.string   "last_name",                limit: 255
    t.boolean  "processing_consent_given", limit: 1,   default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
