#coding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

a = Admin.create(:login => "admin", :name => "Admin Figur", :email => "test@monkeydancers.com", :password => "ninjamagick", :password_confirmation => "ninjamagick", :phone => "46702233340")
a.invitation_accepted_at = Time.now()
a.save

terapeut = Terapeut.new(:login => "tp", :phone => "46702233343", :name => "Terapeut", :email => "terapeut@test.com", :password => "ninjamagick", :password_confirmation => "ninjamagick" )
terapeut.invitation_accepted_at = Time.now()

terapeut.save



patient = terapeut.patients.new(:login => "pt", :phone => "46702233344", :name => "Trångbjörn Valfett", :email => "patient@test.com", :password => "ninjamagick", :password_confirmation => "ninjamagick")
patient.invitation_accepted_at = Time.now()

patient.save
patient.given_permission_to << terapeut
patient.save

def generate_test_data(user, yes, declined, no_answer, period)
  d     = DateTime.now.beginning_of_day  
  yes.times do |time|
  	start_time = d - rand(period).days - rand(24).hours
  	end_time   = start_time + rand(4).hours 
    user.events.create(
    	:name 			=> "Händelse #{time} ja", 
    	:description 	=> "test händelse", 
    	:place 			=>"test plats", 
    	:start 			=> start_time,
    	:end 			=> end_time,
    	:creator 		=> rand(3) == 2 ? user.terapeut : user,
    	:reminder_status 		=> 1,
    	:response_time_in_sec 	=> rand(1800),
    	:num_reminders_required => rand(5),
    	)    
  end
  
	declined.times do |time|
		start_time = d - rand(period).days - rand(24).hours
		end_time   = start_time + rand(4).hours 
		user.events.create(
			:name 			=> "Händelse #{time} nej", 
			:description 	=> "test händelse", 
			:place 			=>"test plats", 
			:start 			=> start_time,
			:end 			=> end_time,
			:creator 		=> rand(3) == 2 ? user.terapeut : user,
			:reminder_status 		=> 2,
			:response_time_in_sec 	=> rand(1800),
			:num_reminders_required => rand(5),
		)    
	end

	no_answer.times do |time|
  		start_time = d - rand(period).days - rand(24).hours
  		end_time   = start_time + rand(4).hours 
	    user.events.create(
	    	:name 			=> "Händelse #{time} inget svar", 
	    	:description 	=> "test händelse", 
	    	:place 			=>"test plats", 
	    	:start 			=> start_time,
	    	:end 			=> end_time,
	    	:creator 		=> rand(3) == 2 ? user.terapeut : user,
	    	:reminder_status 		=> 3,
	    	:response_time_in_sec 	=> rand(1800),
	    	:num_reminders_required => rand(5),
	    )  
	end
end
if patient.save
	generate_test_data(patient, 30,20,5, 30)
else 
	puts "Could not save patient and generate test data"
end

patient.save
100.times do 
	patient.participants.create(:name => Faker::Name.name)
end