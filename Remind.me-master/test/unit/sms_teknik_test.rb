require 'test_helper'
class SMSTeknikTest < ActiveSupport::TestCase

	context 'When sending payload to SMSTeknik, it' do 

		setup do 
			SMSTeknik.testing = false
			FakeWeb.register_uri(:post, "http://smsteknik.se/monkey", :body => "123456")

			@message = TextMessage.create(:sender => 'remind-me', :receiver => '000000000', :body => "Lorem Ipsum...")
		end

		should 'handle a successful response from SMSTeknik' do 
			response = SMSTeknik.post(@message)
			@message.reload
			assert response[:success]
			assert_equal @message.operator_identifier, '123456'
		end

		should 'handle being passed a nil message' do 
			response = SMSTeknik.post(nil)
			assert !response[:success]
			assert_not_nil response[:message]
		end

	end

end