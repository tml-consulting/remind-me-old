require 'test_helper'

class EventTest < ActiveSupport::TestCase

		context 'When editing reminders, an event' do 
			should 'transparently set reminders from its reminder_list attribute' do 
				event = Event.new
				assert !event.persisted? 
				event.reminder_list = {'0' => {:exponent => 10, :multiplier => 'm'}, '1' => {:exponent => 20, :multiplier => 'm'}}
				assert_equal event.reminders.length, 2
				assert !event.reminders.first.persisted?				
			end

			should 'not set reminders if one field is empty' do 
				event = Event.new
				assert !event.persisted? 
				event.reminder_list = {'0' => {:exponent => nil, :multiplier => 'm'}, '1' => {:exponent => 10, :multiplier => ''}}
				assert_equal event.reminders.length, 0
			end

			should 'cleanup old reminders before setting up new ones' do 
				user = create_user
				event = user.events.create(:name => "Apfest", :start => Time.now, :end => Time.now.advance(:hours => 1))
				assert event.reminders.empty? 
				reminder = event.reminders.create(:exponent => 10, :multiplier => 'm')
				assert_not_nil reminder
				assert_not_nil reminder.remind_at
				assert_equal event.reminders.length, 1
				event.reminder_list = {'0' => {:exponent => 10, :multiplier => 'm'}, '1' => {:exponent => 20, :multiplier => 'm'}}
				assert_equal event.reminders.length, 2
				assert !event.reminders.include?(reminder)
			end

			should 'use default reminders if none are set' do 
				user = create_user
				user.default_reminders.create(:exponent => 5, :multiplier => 'h')
				assert_equal user.default_reminders.length, 1
				user.reload
				event = user.events.create(:name => 'Apfest', :start => Time.now, :end => Time.now.advance(:hours => 1))
				assert_equal event.reminders.length, 1
				event_reminder = event.reminders.first
				assert_equal event_reminder.exponent, 5
				assert_equal event_reminder.multiplier, 'h'
			end
		end

		context 'When managing recurrent events, the system' do 
			setup do 
				@user = create_user
			end

			should 'properly create and link weekly recurring events' do 
				assert_equal Event.count, 0
				event = Event.create(:name => "Recurring Test Event", :start => Time.now, :end => Time.now.advance(:hours => 2), :recurrence_key => 'we', :user => @user)
				assert Event.count > 1
				assert event.valid? 
				next_event = Event.where("id > ?", event.id).limit(1).first
				assert_equal event.recurrence_identifier, next_event.recurrence_identifier
				assert_equal next_event.start.day, event.start.advance(:days => 7).day
			end

			should 'properly create and link daily recurring events' do 
				assert_equal Event.count, 0
				event = Event.create(:name => "Recurring Test Event", :start => Time.now, :end => Time.now.advance(:hours => 2), :recurrence_key => 'd', :user => @user)
				assert Event.count > 1
				assert event.valid? 
				next_event = Event.where("id > ?", event.id).limit(1).first
				assert_equal event.recurrence_identifier, next_event.recurrence_identifier
				assert_equal next_event.start.day, event.start.advance(:days => 1).day
			end

			should 'properly create and link weekday-based recurring events' do 
				assert_equal Event.count, 0
				# Make sure we create the event on a weekday, in order to be able to easily verify 
				# that the correct date is calculated
				next_week_day = Time.now
				while [0,6].include?(next_week_day.wday) do 
					next_week_day = next_week_day.advance(:days => 1)
				end
				event = Event.create(:name => "Recurring Test Event", :start => next_week_day, :end => next_week_day.advance(:hours => 2), :recurrence_key => 'wd', :user => @user)
				assert Event.count > 1
				assert event.valid? 
				next_event = Event.where("id > ?", event.id).limit(1).first
				assert_equal event.recurrence_identifier, next_event.recurrence_identifier
				# This is just to make sure tests don't fail based on which day they are actually
				# run on .daniel
				next_correct_date = event.start.advance(:days => 1)
				while [0,6].include?(next_correct_date.wday) do 
					next_correct_date = next_correct_date.advance(:days => 1)
				end
				assert_equal next_event.start.day, next_correct_date.day
			end

			should 'properly update recurring events' do 
				event = Event.create(:name => "Recurring Test Event", :start => Time.now, :end => Time.now.advance(:hours => 2), :recurrence_key => 'we', :user => @user)
				assert Event.count > 1
				assert event.valid? 
				next_event = Event.where("id > ?", event.id).limit(1).first
				assert_equal event.name, next_event.name
				event.update_attributes(:name => "Monkey Name Event")
				next_event.reload
				assert_equal next_event.name, event.name
			end

			should 'properly update dates without changing times' do 
				event = Event.create(:name => "Recurring Test Event", :start => Time.now, :end => Time.now.advance(:hours => 2), :recurrence_key => 'we', :user => @user)
				assert Event.count > 1
				assert event.valid? 
				next_event = Event.where("id > ?", event.id).limit(1).first
				event.update_attributes(:start => Time.now.advance(:hours => 1))
				next_event.reload
				assert_equal event.start.hour, next_event.start.hour
				assert_equal event.start.min, next_event.start.min
				assert_not_equal event.start.day, next_event.start.day
			end

			should 'properly keep deleted events deleted' do 
				event = Event.create(:name => "Recurring Test Event", :start => Time.now, :end => Time.now.advance(:hours => 2), :recurrence_key => 'we', :user => @user)
				assert Event.count > 1				
				assert event.valid? 
				next_event = Event.where("id > ?", event.id).limit(1).first
				next_event.skip_recurring_steps = true
				next_event.destroy				
				next_event_id = next_event.id
				event.start = event.start.advance(:minutes => 10)
				event.save
				new_next_event = Event.where("id > ?", event.id).limit(1).first
				next_event = Event.with_deleted.find(next_event_id)
				assert_not_equal new_next_event.start.strftime("%j"), next_event.start.strftime("%j")
			end

			should 'properly de-link recurring events' do 
				event = Event.create(:name => "Recurring Test Event", :start => Time.now, :end => Time.now.advance(:hours => 2), :recurrence_key => 'we', :user => @user)
				assert Event.count > 1
				assert event.valid? 
				next_event = Event.where("id > ?", event.id).limit(1).first
				event.edit_all_recurring = "false"
				event.update_attributes(:name => "Laserninja")
				next_event.reload
				assert_nil event.recurrence_identifier
				assert_equal event.recurrence_key, 'n'
				assert_not_equal event.name, next_event.name
			end

			should 'delete all recurring instances if asked to' do
				event = Event.create(:name => "Recurring Test Event", :start => Time.now, :end => Time.now.advance(:hours => 2), :recurrence_key => 'we', :user => @user)
				assert Event.count > 1
				assert event.valid? 
				event.destroy
				assert_equal Event.count, 0
			end

			should 'not delete all recurring instances if asked not to' do 
				event = Event.create(:name => "Recurring Test Event", :start => Time.now, :end => Time.now.advance(:hours => 2), :recurrence_key => 'we', :user => @user)
				assert Event.count > 1
				assert event.valid? 
				event.edit_all_recurring = "false"
				assert_difference 'Event.count', -1 do 
					event.destroy
				end
				assert_not_equal Event.count, 0
			end

			should 'not delete previous events of a recurring event' do 
				event = Event.create(:name => "Recurring Test Event", :start => Time.now, :end => Time.now.advance(:hours => 2), :recurrence_key => 'we', :user => @user)
				assert Event.count > 1
				assert event.valid? 
				Timecop.freeze(Date.today + 30)
				event.destroy
				assert_equal Event.count, 4
				assert_not_equal Event.count, 0
			end

		end

		context 'When managing participants, the system' do 
			setup do 
				@user = create_user
			end

			should 'handle participant_list being nil' do 
				event = Event.new(:name => "Monkey", :start => Time.now, :user => @user, :end => Time.now.advance(:hours => 1))
				assert_nil event.participant_list
				event.save
				assert event.valid? 
				assert_empty event.participants
			end
		end

		context "When expiring events, the system" do 
			setup do 
				@user = create_user
				Timecop.freeze(Time.now)
				@event = @user.events.create(:name => "Apfest", :start => Time.now, :end => Time.now.advance(:hours => 1))
			end

			should 'not expire events before 12 hours after end' do
				assert_equal Event.without_answers.count, 0
				Event.expire!(Time.now)
				assert_equal Event.without_answers.count, 0
			end

			should 'properly expire events 12 hours after end' do 
				assert_equal Event.without_answers.count, 0
				Timecop.freeze(Time.now.advance(:hours => 13))
				Event.expire!(Time.now)
				assert_equal Event.without_answers.count, 1
				@event.reload
				assert @event.no_answer?
			end

			teardown do
				Timecop.return
			end

		end
end
