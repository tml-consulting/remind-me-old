require 'test_helper'

class ReminderTest < ActiveSupport::TestCase

	context 'When creating reminders, the system' do 
		setup do 
			@user = create_user
		end

		should 'handle start-date being nil on the event' do 
			event = @user.events.create(:name => "Apfest", :start => Time.now)
			assert_nothing_raised do 
				event.reminder_list = {'0' => {:exponent => 10, :multiplier => 'm'}, '1' => {:exponent => 20, :multiplier => 'm'}}
			end
		end
	end

	context 'When reminding reminders, the system' do 
		setup do 
			@user = create_user
			@event = Event.create(
				:user => @user, 
				:name => "Test Event", 
				:start => Time.now.advance(:days => 1), 
				:end => Time.now.advance(:days => 1, :hours => 2), 
				:reminder_list => {'0' => {:exponent => '12', :multiplier => 'm'}, '1' => {:exponent => '5', :multiplier => 'h'}})
			SMSTeknik.testing = true
		end

		should 'only load due reminders' do 
			status = Reminder.send_reminders(Time.now)
			assert_empty status[:completed_reminders]
			assert_empty status[:faulty_reminders]
			assert status[:flawless_run]
			Timecop.freeze(@event.start.advance(:hours => -5, :minutes => 2))
			status = Reminder.send_reminders(Time.now.utc)
			assert_equal status[:completed_reminders].last, Reminder.last
			assert_empty status[:faulty_reminders]
			assert status[:flawless_run]
			Timecop.freeze(@event.start.advance(:minutes => -11))
			status = Reminder.send_reminders(Time.now.utc)
			assert_equal status[:completed_reminders].last, Reminder.first
			assert_empty status[:faulty_reminders]
			assert status[:flawless_run]
		end

		teardown do 
			Timecop.return
		end
	end

end
