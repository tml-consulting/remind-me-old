require 'test_helper'

class UserTest < ActiveSupport::TestCase

	context 'When inviting users, the system' do 

		setup do 
			@terapeut = create_terapeut
		end

		should 'pre-format phone numbers even when creation is done by invitable' do 
			user = @terapeut.patients.invite!({:email => "monkey@monkeydancers.com", :name => "test", :phone => "070-111 22 33"}, @terapeut) do |p|
				p.given_permission_to << @terapeut 
				p.terapeut = @terapeut
			end
			assert user.persisted? 
			assert_equal user.phone, "46701112233"
		end

	end

end
