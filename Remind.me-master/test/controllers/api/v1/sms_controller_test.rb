require 'test_helper'

class Api::V1::SmsControllerTest < ActionController::TestCase

	context 'When receiving status updates, the system' do 
		setup do 
			message = TextMessage.new(:sender => "0702267868", :receiver => "000000000", :body => "Lorem ipsum")
			message.operator_identifier = "test-identifier"
			message.save			
		end

		should 'properly update the status' do 
			message = TextMessage.last
			post :status, {:customid => message.custom_message_id, :ref => "test-identifier", :datetime => Time.now.to_s(:db), :state => "DELIVRD"}
			assert_response 200
			assert_not_nil message.statuses.last
			assert_equal message.statuses.last.operator_status, "DELIVRD"
		end
	end


	context 'When receiving incoming text messages, it' do 
		setup do 
			@user = create_user
			@event = Event.create(:user => @user, :name => "Test Event", :start => Time.now, :end => Time.now.advance(:hours => 2), :reminder_list => {'0' => {:exponent => 'm', :multiplier => '12'}})
			SMSTeknik.testing = true
		end

		should 'properly manage the response chain' do 
			assert @event.valid? 
			assert_equal @event.reminders.length, 1
			assert_equal TextMessage.all.length, 0
			assert_equal @event.num_reminders_required, nil
			assert_equal @event.response_time_in_sec, nil
			reminder = @event.reminders.last
			reminder.send_reminder
			reminder.reload
			assert reminder.awaiting_response?
			post :receive_sms, {:orgaddr => '46702267868', :destaddr => '000000000', :text => "ja #{reminder.message_identifier}"}
			reminder.reload
			@event.reload
			assert_response 200
			assert reminder.finished? 
			response_message = TextMessage.last
			assert response_message.incoming? 
			assert_equal TextMessage.all.length, 2
			assert_equal @event.num_reminders_required, 1
			assert reminder.text_messages.include?(response_message)
			assert @event.answer_yes?
		end

		should 'properly handle misspellings or mistakes in response for yes' do 
			assert @event.valid? 
			assert_equal @event.reminders.length, 1
			assert_equal TextMessage.all.length, 0
			assert_equal @event.num_reminders_required, nil
			assert_equal @event.response_time_in_sec, nil
			reminder = @event.reminders.last
			reminder.send_reminder
			reminder.reload
			assert reminder.awaiting_response?
			post :receive_sms, {:orgaddr => '46702267868', :destaddr => '000000000', :text => "aj #{reminder.message_identifier}"}
			reminder.reload
			@event.reload
			assert_response 200
			assert reminder.finished? 
			response_message = TextMessage.last
			assert response_message.incoming? 
			assert_equal TextMessage.all.length, 2
			assert_equal @event.num_reminders_required, 1
			assert_equal @event.response_time_in_sec, 0
			assert reminder.text_messages.include?(response_message)
			assert @event.answer_yes?
		end

		should 'properly handle misspellings or mistakes in response for no' do 
			assert @event.valid? 
			assert_equal @event.reminders.length, 1
			assert_equal TextMessage.all.length, 0
			assert_equal @event.num_reminders_required, nil
			assert_equal @event.response_time_in_sec, nil
			reminder = @event.reminders.last
			reminder.send_reminder
			reminder.reload
			assert reminder.awaiting_response?
			post :receive_sms, {:orgaddr => '46702267868', :destaddr => '000000000', :text => "nej #{reminder.message_identifier}"}
			reminder.reload
			@event.reload
			assert_response 200
			assert reminder.finished? 
			response_message = TextMessage.last
			assert response_message.incoming? 
			assert_equal TextMessage.all.length, 2
			assert_equal @event.num_reminders_required, 1
			assert_equal @event.response_time_in_sec, 0
			assert reminder.text_messages.include?(response_message)
			assert @event.answer_no?
		end

		should 'properly handle misspellings or mistakes in response for jne' do 
			assert @event.valid? 
			assert_equal @event.reminders.length, 1
			assert_equal TextMessage.all.length, 0
			assert_equal @event.num_reminders_required, nil
			assert_equal @event.response_time_in_sec, nil
			reminder = @event.reminders.last
			reminder.send_reminder
			reminder.reload
			assert reminder.awaiting_response?
			post :receive_sms, {:orgaddr => '46702267868', :destaddr => '000000000', :text => "jne #{reminder.message_identifier}"}
			reminder.reload
			@event.reload
			assert_response 200
			assert reminder.finished? 
			response_message = TextMessage.last
			assert response_message.incoming? 
			assert_equal TextMessage.all.length, 2
			assert_equal @event.num_reminders_required, 1
			assert @event.response_time_in_sec < 5
			assert reminder.text_messages.include?(response_message)
			assert @event.answer_no?
		end

		should 'properly handle misspellings or mistakes in response for enj' do 
			assert @event.valid? 
			assert_equal @event.reminders.length, 1
			assert_equal TextMessage.all.length, 0
			assert_equal @event.num_reminders_required, nil
			assert_equal @event.response_time_in_sec, nil
			reminder = @event.reminders.last
			reminder.send_reminder
			reminder.reload
			assert reminder.awaiting_response?
			post :receive_sms, {:orgaddr => '46702267868', :destaddr => '000000000', :text => "enj #{reminder.message_identifier}"}
			reminder.reload
			@event.reload
			assert_response 200
			assert reminder.finished? 
			response_message = TextMessage.last
			assert response_message.incoming? 
			assert_equal TextMessage.all.length, 2
			assert_equal @event.num_reminders_required, 1
			assert_equal @event.response_time_in_sec, 0
			assert reminder.text_messages.include?(response_message)
			assert @event.answer_no?
		end

		should 'properly handle missing spaces in response for no' do 
			assert @event.valid? 
			assert_equal @event.reminders.length, 1
			assert_equal TextMessage.all.length, 0
			assert_equal @event.num_reminders_required, nil
			assert_equal @event.response_time_in_sec, nil
			reminder = @event.reminders.last
			reminder.send_reminder
			reminder.reload
			assert reminder.awaiting_response?
			post :receive_sms, {:orgaddr => '46702267868', :destaddr => '000000000', :text => "enj#{reminder.message_identifier}"}
			reminder.reload
			@event.reload
			assert_response 200
			assert reminder.finished? 
			response_message = TextMessage.last
			assert response_message.incoming? 
			assert_equal TextMessage.all.length, 2
			assert_equal @event.num_reminders_required, 1
			assert [0,1].include?(@event.response_time_in_sec)
			assert reminder.text_messages.include?(response_message)
			assert @event.answer_no?
		end

		should 'properly handle missing spaces in response for yes' do 
			assert @event.valid? 
			assert_equal @event.reminders.length, 1
			assert_equal TextMessage.all.length, 0
			assert_equal @event.num_reminders_required, nil
			assert_equal @event.response_time_in_sec, nil
			reminder = @event.reminders.last
			reminder.send_reminder
			reminder.reload
			assert reminder.awaiting_response?
			post :receive_sms, {:orgaddr => '46702267868', :destaddr => '000000000', :text => "aj#{reminder.message_identifier}"}
			reminder.reload
			@event.reload
			assert_response 200
			assert reminder.finished? 
			response_message = TextMessage.last
			assert response_message.incoming? 
			assert_equal TextMessage.all.length, 2
			assert_equal @event.num_reminders_required, 1
			assert_equal @event.response_time_in_sec, 0
			assert reminder.text_messages.include?(response_message)
			assert @event.answer_yes?
		end
	end

end
