ENV["RAILS_ENV"] = "test"
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require 'shoulda-context'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.(yml|csv) for all tests in alphabetical order.
  #

  def create_user(options = {})
  	user = User.create({:email => "test@monkeydancers.com", :password => "ninjamagick", :password_confirmation => "ninjamagick", 
  		:phone => '00000000'}.merge(options))
  	user
  end

  def create_terapeut(options = {})
  	user = Terapeut.create({:email => "test@monkeydancers.com", :password => "ninjamagick", :password_confirmation => "ninjamagick", 
  		:phone => '00000000'}.merge(options))
  	user
  end

end
