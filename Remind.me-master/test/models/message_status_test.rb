require 'test_helper'

class MessageStatusTest < ActiveSupport::TestCase

	context 'When creating new MessageStatus instances, it' do 

		should 'verify that operator status is a valid one' do 
			status = MessageStatus.new(:operator_status => "monkey")
			assert !status.valid? 
			assert_not_empty status.errors[:operator_status]
			status.operator_status = SMSTeknik::ALLOWED_MESSAGE_STATES.first
			assert status.valid? 
		end

	end

end
