require 'test_helper'

class TextMessageTest < ActiveSupport::TestCase

	context 'When creating TextMessages, it' do 
		should 'properly set a custom message id' do 
			message = TextMessage.new
			assert !message.valid? 
			assert_not_nil message.custom_message_id
			assert_empty message.errors[:custom_message_id]
		end

		should 'require certain data' do 
			message = TextMessage.new(:sender => nil, :receiver => "non-numerical")
			assert !message.valid?
			assert_not_empty message.errors[:receiver]
		end		

		should 'be persisted if passed proper data' do 
			message = TextMessage.new(:sender => "remind-me", :receiver => "000000000", :body => "Lorem ipsum")
			assert message.valid? 
			message.save
			assert message.persisted? 
			assert_not_nil message.custom_message_id			
		end

		should 'not set custom id of message is incoming' do 
			message = TextMessage.new(:sender => "remind-me", :receiver => "000000000", :body => "Lorem ipsum", :incoming => true)
			assert message.valid?
			assert_nil message.custom_message_id
		end
	end

	context 'When creating messages using shortcuts, it' do 
		setup do 
			SMSTeknik.testing = false
			FakeWeb.register_uri(:post, "http://smsteknik.se/monkey", :body => "123456")
		end

		should 'create and send a message' do 
			message = TextMessage.send_message("0702267868", "Test Message...")
			assert_not_nil message
			message.reload
			assert_equal message.operator_identifier, "123456"			
		end
	end

	context 'When sending TextMessage via SMSTeknik, it' do 
		setup do 
			SMSTeknik.testing = true
		end

		should 'be properly sent' do 
			t = TextMessage.create(:sender => 'remind-me', :receiver => '000000000', :body => "Lorem Ipsum...")
			response = SMSTeknik.post(t)
			t.reload
			assert response[:success]
			assert_equal t.operator_identifier, 'test-response'
		end
	end

end
