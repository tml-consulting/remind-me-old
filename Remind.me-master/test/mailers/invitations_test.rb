require 'test_helper'

class InvitationsTest < ActionMailer::TestCase

	context 'When sending an invitation, the system' do 
		setup do 
			@terapeut = create_terapeut({:email => "terapeut@test.com", :name => "Monkey"})
			@user = create_user({:terapeut => @terapeut})
		end

		should 'correctly send an email' do 
			email = Invitations.invitation_instructions(@user, @user.invitation_token)
			email.deliver_now
			assert !ActionMailer::Base.deliveries.empty?
			assert email.html_part.body.match(configatron.app.url)
			assert email.text_part.body.match(configatron.app.url)
			assert_equal email.from, ['robot@monkeydancers.com']
			assert_equal email.to, [@user.email]
		end
	end

end
